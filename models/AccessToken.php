<?php

namespace azbuco\user\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "access_token".
 *
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property string $name
 * @property string $key
 * @property int $created_at
 */
class AccessToken extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%access_token}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'token'], 'required'],
            [['user_id', 'created_at'], 'integer'],
            [['token', 'name', 'key'], 'string', 'max' => 255],
            [['token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('azbuco.user', 'ID'),
            'user_id' => Yii::t('azbuco.user', 'User ID'),
            'token' => Yii::t('azbuco.user', 'Token'),
            'name' => Yii::t('azbuco.user', 'Name'),
            'key' => Yii::t('azbuco.user', 'Key'),
            'created_at' => Yii::t('azbuco.user', 'Created At'),
        ];
    }

}
