<?php

namespace azbuco\user\models;

use azbuco\user\components\ConfirmedEmailValidator;
use azbuco\user\models\User;
use azbuco\user\UserModule;
use Yii;
use yii\base\Model;
use yii\db\IntegrityException;

/**
 * Signup form
 * 
 * @property UserModule $module
 */
class SignupForm extends Model {

    /**
     * @var string User real name
     */
    public $name;

    /**
     * @var string User email address
     */
    public $email;

    /**
     * @var string Password for the user
     */
    public $password;

    /**
     * @var boolean 
     */
    public $accept;

    /**
     * @var integer Minimum length of accepted password during signup or password reset.
     */
    protected $_passwordMinLength = 6;

    /**
     * @var callable A callable for complex password validation (ex: require non alphanumeric character)
     */
    protected $_passwordValidator;

    /**
     * @var integer Whether validate the user email as confirmed or not.
     */
    protected $_validateForEmailConfirmation;

    /**
     * @var integer Whether the accept parameter is required or not.
     */
    protected $_validateForAcceptTerms;

    /**
     * @var string Label for the accept checkbox
     */
    protected $_acceptLabel;

    public function __construct(UserModule $module, $config = array())
    {
        $this->_passwordMinLength = $module->passwordMinLength;
        $this->_passwordValidator = $module->passwordValidator;
        $this->_validateForEmailConfirmation = $module->requireEmailConfirmation;
        $this->_validateForAcceptTerms = $module->requireAcceptTerms;
        $this->_acceptLabel = $module->acceptTermsLabel;

        parent::__construct($config);
    }

    public function hasAccept()
    {
        return $this->_validateForAcceptTerms;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['name', 'email', 'password'],
                'required'
            ],
            [
                ['name', 'email'],
                'trim'
            ],
            [
                'email',
                'email'
            ],
            [
                'name',
                'string',
                'min' => 1,
                'max' => 255
            ],
            [
                'password',
                'string',
                'min' => $this->_passwordMinLength,
                'max' => 45
            ],
            [
                'password',
                function ($attribute, $params, $validator) {
                    $callable = $this->_passwordValidator;
                    if (is_callable($callable)) {
                        call_user_func($callable, $this, $attribute, $params, $validator);
                    }
                }
            ],
            [
                'email',
                ConfirmedEmailValidator::class,
                'when' => function() {
                    return $this->_validateForEmailConfirmation === true;
                }
            ],
            [
                'email',
                'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('azbuco.user', 'Email is already taken.'),
            ],
            [
                'accept',
                'compare',
                'compareValue' => 1,
                'message' => Yii::t('azbuco.user', 'You have to accept the Terms and Conditions.'),
                'when' => function() {
                    return $this->_validateForAcceptTerms === true;
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('azbuco.user', 'Name'),
            'email' => Yii::t('azbuco.user', 'Email'),
            'password' => Yii::t('azbuco.user', 'Password'),
            'accept' => $this->_acceptLabel,
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|false the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = new User();
        $user->name = $this->name;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        // Always generate email confirmation token, maybe we dont use it later.
        $user->generateEmailConfirmationToken();

        $result = $user->save();
        if (!$result) {
            throw new IntegrityException('Unable to save User.');
        }

        return $user;
    }

}
