<?php

namespace azbuco\user\models;

use azbuco\eav\EavInterface;
use azbuco\eav\EavTrait;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_options}}".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $key
 * @property int|null $int_value
 * @property float|null $num_value
 * @property string|null $string_value
 * @property string|null $text_value
 * @property string|null $json_value
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property User $user
 */
class UserOptions extends ActiveRecord implements EavInterface
{

    use EavTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_options}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'int_value', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['num_value'], 'number'],
            [['text_value', 'json_value'], 'string'],
            [['key', 'string_value'], 'string', 'max' => 255],
            [['user_id', 'key'], 'unique', 'targetAttribute' => ['user_id', 'key']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'key' => 'Key',
            'int_value' => 'Int Value',
            'num_value' => 'Num Value',
            'string_value' => 'String Value',
            'text_value' => 'Text Value',
            'json_value' => 'Json Value',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::class,
            ],
            'blameableBehavior' => [
                'class' => BlameableBehavior::class,
            ]
        ];
    }

}
