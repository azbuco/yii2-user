<?php

namespace azbuco\user\models;

use azbuco\user\components\ConfirmedEmailValidator;
use azbuco\user\UserModule;
use Yii;
use yii\base\Model;

/**
 * Login form
 * 
 * @property UserModule $module
 */
class LoginForm extends Model {

    public $email;
    public $password;
    public $rememberMe = true;

    /**
     * @var User The user model
     */
    protected $_user;

    /**
     * @var UserModule To access the module settings
     */
    protected $_module;

    public function __construct(UserModule $module, $config = array())
    {
        $this->_module = $module;

        parent::__construct($config);
    }

    public function getModule()
    {
        return $this->_module;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['email', 'password'],
                'required',
            ],
            [
                'email',
                ConfirmedEmailValidator::class, 'when' => function() {
                    return $this->module->requireEmailConfirmation === true;
                },
            ],
            [
                'rememberMe',
                'boolean',
            ],
            [
                'password',
                'validatePassword',
            ],
            [
                // this is the last validator, only if password is ok
                'email',
                \azbuco\user\components\BlockedEmailValidator::class,
            ]
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('azbuco.user', 'Email'),
            'password' => Yii::t('azbuco.user', 'Password'),
            'rememberMe' => Yii::t('azbuco.user', 'Remember Me'),
        ];
    }
    

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('azbuco.user', 'Incorrect email or password.'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }

    /**
     * Finds user by email
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

}
