<?php

namespace azbuco\user\models;

use azbuco\user\UserModule;
use yii\base\InvalidParamException;
use yii\base\Model;

/**
 * Password reset form
 */
class PasswordSetForm extends Model {

    public $password;

    /**
     * @var User
     */
    protected $_user;

    /**
     * @var integer Minimum length of accepted password during signup or password reset.
     */
    protected $_passwordMinLength = 6;

    /**
     * @var callable A callable for complex password validation (ex: require non alphanumeric character)
     */
    protected $_passwordValidator;

    /**
     * Creates a form model given a token.
     *
     * @param UserModule $module
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws InvalidParamException if token is empty or not valid
     */
    public function __construct(UserModule $module, $token, $config = [])
    {
        $this->_passwordMinLength = $module->passwordMinLength;
        $this->_passwordValidator = $module->passwordValidator;

        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'password',
                'string',
                'min' => $this->_passwordMinLength,
                'max' => 45
            ],
            [
                'password',
                function ($attribute, $params, $validator) {
                    $callable = $this->_passwordValidator;
                    if (is_callable($callable)) {
                        call_user_func($callable, $this, $attribute, $params, $validator);
                    }
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $user = new User;

        return [
            'password' => $user->getAttributeLabel('password'),
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;

        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }

}
