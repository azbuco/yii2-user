<?php

namespace azbuco\user\models;

use Yii;
use yii\base\InvalidCallException;
use yii\base\Model;
use yii\db\IntegrityException;
use yii\db\Query;
use yii\gii\Module;

/**
 * Signup form
 * 
 * @property Module $module
 */
class UserForm extends Model
{

    const SCENARIO_CREATE = 'scenario_create';
    const SCENARIO_UPDATE = 'scenario_update';

    /**
     * @var string User real name
     */
    public $name;

    /**
     * @var string User email address
     */
    public $email;

    /**
     * @var string Password for the user
     */
    public $password;

    /**
     * @var string User roles
     */
    public $roles = [];

    /**
     * @var User The updated user
     */
    protected $_user;

    /**
     * @var integer Minimum length of accepted password during signup or password reset.
     */
    protected $_passwordMinLength = 6;

    /**
     * @var callable A callable for complex password validation (ex: require non alphanumeric character)
     */
    protected $_passwordValidator;

    /**
     * @param User $user
     * @param Module $module
     * @param array $config
     */
    public function __construct($user, $module, $config = array())
    {
        if ($user === null) {
            $this->scenario = self::SCENARIO_CREATE;
        } else {
            $this->_user = $user;
            $this->scenario = self::SCENARIO_UPDATE;
            $this->email = $user->email;
            $this->name = $user->name;
            $roles = array_keys(Yii::$app->authManager->getRolesByUser($user->id));
            $this->roles = $roles;
        }

        $this->_passwordMinLength = $module->passwordMinLength;
        $this->_passwordValidator = $module->passwordValidator;

        parent::__construct($config);
    }

    // get scenarios
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['name', 'email', 'password', 'roles'],
            self::SCENARIO_UPDATE => ['name', 'email', 'password', 'roles'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = [
            [
                ['name', 'email'],
                'required'
            ],
            [
                ['password'],
                'required',
                'on' => self::SCENARIO_CREATE,
            ],
            [
                ['name', 'email'],
                'trim'
            ],
            [
                'name',
                'string',
                'min' => 1,
                'max' => 255
            ],
            [
                'password',
                'string',
                'min' => $this->_passwordMinLength,
                'max' => 45
            ],
            [
                'email',
                'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('azbuco.user', 'Email is already taken.'),
                'on' => self::SCENARIO_CREATE,
            ],
            [
                'email',
                'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('azbuco.user', 'Email is already taken.'),
                'on' => self::SCENARIO_UPDATE,
                'filter' => function($query) {
                    /* @var $query Query */
                    return $query->andWhere(['!=', 'id', $this->_user->id]);
                }
            ],
            [
                'roles',
                'each',
                'rule' => [
                    'in',
                    'range' => function() {
                        return array_keys(Yii::$app->authManager->getRoles());
                    },
                ],
            ],
        ];

        if (!Yii::$app->controller->module->enableInvalidEmail) {
            $rules[] = ['email', 'email'];
        }

        return $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('azbuco.user', 'Name'),
            'email' => Yii::t('azbuco.user', 'Email'),
            'password' => Yii::t('azbuco.user', 'Password'),
            'roles' => Yii::t('azbuco.user', 'Roles'),
        ];
    }

    public function getUser()
    {
        return $this->_user;
    }

    public function save()
    {
        if ($this->scenario === self::SCENARIO_CREATE) {
            return $this->create();
        }
        if ($this->scenario === self::SCENARIO_UPDATE) {
            return $this->update();
        }

        throw new InvalidCallException('Invalid scenario.');
    }

    /**
     * Create a new user
     *
     * @return User|false the saved model or null if saving fails
     */
    public function create()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = new User();
        $user->name = $this->name;
        $user->email = $this->email;
        if ($this->password) {
            $user->setPassword($this->password);
        }
        $user->generateAuthKey();
        $user->generateEmailConfirmationToken();

        if (!$user->save()) {
            throw new IntegrityException('Unable to save User.');
        }

        $this->setRoles($user);

        return $user;
    }

    /**
     * @param User $user
     * @return false|User False if validation failed, User object if save success.
     * @throws IntegrityException
     */
    public function update()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->_user->name = $this->name;
        $this->_user->email = $this->email;

        if (!$this->_user->save()) {
            throw new IntegrityException('Unable to save User.');
        }

        if ($this->password) {
            $this->_user->setPassword($this->password);
            $this->_user->update(false, ['password_hash']);
        }

        $this->setRoles($this->_user);

        return $this->_user;
    }

    protected function setRoles($user)
    {
        $authManager = Yii::$app->authManager;
        $authManager->revokeAll($user->id);

        if (empty($this->roles)) {
            return;
        }

        foreach ($this->roles as $roleName) {
            $role = $authManager->getRole($roleName);
            $authManager->assign($role, $user->id);
        }
    }

}
