<?php

namespace azbuco\user\models;

use Yii;
use yii\base\Model;
use yii\web\ServerErrorHttpException;

class PasswordChangeForm extends Model {

    /**
     * @var string
     */
    public $password_current;

    /**
     * @var string
     */
    public $password_new;

    /**
     * @var string
     */
    public $password_repeat;

    /**
     * @var User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password_current', 'password_new', 'password_repeat'], 'required'],
            ['password_current', 'validatePassword'],
            [['password_new', 'password_repeat'], 'string', 'min' => 6, 'max' => 45],
            [
                'password_repeat',
                'compare',
                'compareAttribute' => 'password_new',
                'message' => Yii::t('azbuco.user', 'The passwords don\'t match.'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password_current' => Yii::t('azbuco.user', 'Current Password'),
            'password_new' => Yii::t('azbuco.user', 'New Password'),
            'password_repeat' => Yii::t('azbuco.user', 'Repeat New Password'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->_user = User::findOne(Yii::$app->user->id);
        if (!$this->_user) {
            throw new ServerErrorHttpException('Invalid user.');
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            if (!$this->_user || !$this->_user->validatePassword($this->$attribute)) {
                $this->addError($attribute, Yii::t('azbuco.user', 'Incorrect password.'));
            }
        }
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->_user->setPassword($this->password_new);

        $result = $this->_user->update(false, ['password_hash']);
        if ($result === false) {
            throw new ServerErrorHttpException(Yii::t('azbuco.user', 'Unable to save new password.'));
        }
        
        return $this->_user;
    }

}
