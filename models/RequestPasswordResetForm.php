<?php

namespace azbuco\user\models;

use azbuco\user\components\ConfirmedEmailValidator;
use azbuco\user\models\User;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class RequestPasswordResetForm extends Model {

    public $email;

    /**
     * @var boolean Whether validate email as confirmed or not.
     */
    protected $_validateForEmailConfirmation;

    public function __construct($validateForEmailConfirmation, $config = array())
    {
        $this->_validateForEmailConfirmation = $validateForEmailConfirmation;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'exist',
                'targetClass' => Yii::$app->user->identityClass,
                'targetAttribute' => 'email',
                'filter' => ['blocked_at' => null],
            ],
            ['email', ConfirmedEmailValidator::class, 'when' => function() {
                    return $this->_validateForEmailConfirmation;
                }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        $user = new User;

        return [
            'email' => $user->getAttributeLabel('email'),
        ];
    }

}
