<?php

namespace azbuco\user\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserSearch extends User {

    public $role;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'updated_at', 'created_at'], 'integer'],
            [['name', 'email', 'confirmed_at', 'blocked_at', 'deleted_at', 'password_hash', 'auth_key', 'password_reset_token', 'email_confirmation_token', 'registration_ip'], 'safe'],
            ['role', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = User::find();


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        if ($this->confirmed_at === 'true') {
            $query->andWhere(['NOT', ['confirmed_at' => null]]);
        } else if ($this->confirmed_at === 'false') {
            $query->andWhere(['confirmed_at' => null]);
        }

        if ($this->deleted_at === 'true') {
            $query->andWhere(['NOT', ['deleted_at' => null]]);
        } else if ($this->deleted_at === 'false') {
            $query->andWhere(['deleted_at' => null]);
        }

        if ($this->blocked_at === 'true') {
            $query->andWhere(['NOT', ['blocked_at' => null]]);
        } else if ($this->blocked_at === 'false') {
            $query->andWhere(['blocked_at' => null]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email]);


        if ($this->role) {
            $allRoles = \Yii::$app->authManager->getRoles();
            $roles = [];
            foreach ($allRoles as $role) {
                $childRoles = \Yii::$app->authManager->getChildRoles($role->name);
                foreach ($childRoles as $childRole) {
                    if ($childRole->name === $this->role) {
                        $roles[] = $role->name;
                    }
                }
            }

            $userIds = [];
            foreach ($roles as $role) {
                $ids = \Yii::$app->authManager->getUserIdsByRole($role);
                $userIds = array_merge($userIds, $ids);
            }

            $query->andWhere(['id' => $userIds]);
        }

        return $dataProvider;
    }

}
