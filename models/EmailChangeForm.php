<?php

namespace azbuco\user\models;

use Yii;
use yii\base\Model;
use yii\web\ServerErrorHttpException;

class EmailChangeForm extends Model {

    /**
     * @var string
     */
    public $email_new;

    /**
     * @var User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'email_new',
                'required'
            ],
            [
                'email_new',
                'email'
            ],
            [
                'email_new',
                'unique',
                'targetClass' => User::className(),
                'targetAttribute' => 'email',
                'message' => Yii::t('azbuco.user', 'Email is already taken.'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email_new' => Yii::t('azbuco.user', 'New Email'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->_user = User::findOne(Yii::$app->user->id);
        if (!$this->_user) {
            throw new ServerErrorHttpException('Invalid user.');
        }
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->_user->email = $this->email_new;

        $result = $this->_user->update(false, ['email']);
        if ($result === false) {
            throw new ServerErrorHttpException(Yii::t('azbuco.user', 'Unable to save new email.'));
        }

        return $this->_user;
    }

}
