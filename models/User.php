<?php

namespace azbuco\user\models;

use azbuco\eav\HasOptionInterface;
use azbuco\eav\HasOptionTrait;
use azbuco\user\components\UserInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $email_confirmation_token
 * @property string $registration_ip
 * @property int $confirmed_at
 * @property int $blocked_at
 * @property int $deleted_at
 * @property int $updated_at
 * @property int $created_at
 *
 * @property AccessToken[] $accessTokens
 */
class User extends ActiveRecord implements IdentityInterface, UserInterface, HasOptionInterface
{
    use HasOptionTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'password_hash', 'auth_key'], 'required'],
            [['confirmed_at', 'blocked_at', 'deleted_at', 'updated_at', 'created_at'], 'integer'],
            [['name', 'email', 'password_reset_token', 'email_confirmation_token'], 'string', 'max' => 255],
            [['password_hash'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 45],
            [['email'], 'unique'],
            [['email', 'password_hash', 'auth_key'], 'unique', 'targetAttribute' => ['email', 'password_hash', 'auth_key']],
            [['password_reset_token', 'email_confirmation_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('azbuco.user', 'ID'),
            'name' => Yii::t('azbuco.user', 'Name'),
            'email' => Yii::t('azbuco.user', 'Email'),
            'password_hash' => Yii::t('azbuco.user', 'Password Hash'),
            'auth_key' => Yii::t('azbuco.user', 'Auth Key'),
            'password_reset_token' => Yii::t('azbuco.user', 'Password Reset Token'),
            'email_confirmation_token' => Yii::t('azbuco.user', 'Email Confirm Token'),
            'registration_ip' => Yii::t('azbuco.user', 'Registration IP'),
            'confirmed_at' => Yii::t('azbuco.user', 'Confirmed At'),
            'blocked_at' => Yii::t('azbuco.user', 'Blocked At'),
            'deleted_at' => Yii::t('azbuco.user', 'Deleted At'),
            'updated_at' => Yii::t('azbuco.user', 'Updated At'),
            'created_at' => Yii::t('azbuco.user', 'Created At'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::findOne([
            'id' => $id,
            'blocked_at' => null,
            'deleted_at' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     * @return static|null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()
            ->active()
            ->leftJoin('{{%access_token}}', '{{%access_token}}.user_id = {{%user}}.id')
            ->where(['{{%access_token}}.token' => $token])
            ->one();
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return self::find()
            ->active()
            ->andWhere(['email' => $email])
            ->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!self::isTokenValid($token)) {
            return null;
        }

        return self::find()
            ->active()
            ->andWhere([
                'password_reset_token' => $token,
            ])
            ->one();
    }

    /**
     * Finds user by email confirmation token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByEmailConfirmationToken($token)
    {
        return self::find()
            ->active()
            ->andWhere([
                'email_confirmation_token' => $token,
            ])
            ->one();
    }

    /**
     * Finds all active user
     *
     * @return User[]
     */
    public static function findActive()
    {
        return self::find()
            ->active()
            ->all();
    }

    /**
     * Find users by their roles
     *
     * @param string|array $roles
     * @return User[]
     */
    public static function findByRole($roles)
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }

        $ids = [];
        foreach ($roles as $role) {
            $ids = array_merge($ids, Yii::$app->authManager->getUserIdsByRole($role));
        }

        return self::find()
            ->active()
            ->andWhere(['id' => $ids])
            ->orderBy(['name' => SORT_ASC])
            ->all();
    }

    /**
     * Finds out if a token is expired.
     *
     * @param string $token token
     * @param int $expire Expiration time in seconds, default 7200 (60*60*2)
     * @return bool
     */
    public static function isTokenValid($token, $expire = 7200)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Generates new email confirmatioin token
     */
    public function generateEmailConfirmationToken()
    {
        $this->email_confirmation_token = Yii::$app->security->generateRandomString();
    }

    public function getAccessTokens()
    {
        return $this->hasMany(AccessToken::class, ['user_id' => 'id']);
    }

    public function isActive()
    {
        return ($this->blocked_at === null && $this->deleted_at === null) ? true : false;
    }

    public function isBlocked()
    {
        return $this->blocked_at === null ? false : true;
    }

    public function isDeleted()
    {
        return $this->deleted_at === null ? false : true;
    }

    public function optionModelClass()
    {
        return UserOptions::class;
    }

    public function optionModelLinkAttribute()
    {
        return 'user_id';
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

}
