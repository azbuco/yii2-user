<?php

use azbuco\user\models\EmailChangeForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model EmailChangeForm */

$this->title = Yii::t('azbuco.user', 'Change Email');
?>

<div class="d-flex justify-content-center w-100">
    <div class="col-md-6 col-lg-4 p-0">
        <div class="card my-5">
            <div class="card-body">

                <h5 class="text-center mb-3">
                    <?= $this->title ?>
                </h5>

                <?php
                $form = ActiveForm::begin([
                    'id' => 'form-email-change',
                    'enableClientValidation' => false,
                ]);
                ?>

                <p>
                    <?= Yii::t('azbuco.user', 'You are going to change your email address from <span>{email}</span> to...', ['email' => Yii::$app->user->identity->email]) ?>
                </p>

                <?= $form->field($model, 'email_new')->textInput(['autofocus' => true]) ?>

                <div class="form-group text-center mb-0 mt-3">
                    <?=
                    Html::submitButton(Yii::t('azbuco.user', 'Change Email'), [
                        'class' => 'btn btn-primary mb-0',
                    ])
                    ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

