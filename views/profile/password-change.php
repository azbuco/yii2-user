<?php

use azbuco\user\models\PasswordChangeForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model PasswordChangeForm */

$this->title = Yii::t('azbuco.user', 'Change Password');
?>

<div class="d-flex justify-content-center w-100">
    <div class="col-md-6 col-lg-4 p-0">
        <div class="card my-5">
            <div class="card-body">

                <h5 class="text-center mb-3">
                    <?= $this->title ?>
                </h5>

                <?php
                $form = ActiveForm::begin([
                    'id' => 'form-password-change',
                    'enableClientValidation' => false,
                ]);
                ?>

                <?= $form->field($model, 'password_current')->passwordInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password_new')->passwordInput() ?>

                <?= $form->field($model, 'password_repeat')->passwordInput() ?>


                <div class="form-group text-center mb-0 mt-3">
                    <?=
                    Html::submitButton(Yii::t('azbuco.user', 'Change Password'), [
                        'class' => 'btn btn-primary mb-0',
                    ])
                    ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

