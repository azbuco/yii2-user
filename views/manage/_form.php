<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model azbuco\user\common\models\User */
/* @var $form ActiveForm */

$roles = Yii::$app->authManager->getRoles();

$groups = [];
foreach ($roles as $role) {
    $parts = explode(':', $role->description);
    $module = $parts[0];
    $name = isset($parts[1]) ? $parts[1] : $parts[0];

    if (!isset($groups[$module])) {
        $groups[$module] = [];
    }
    $groups[$module][$role->name] = $role;
}

foreach ($groups as &$group) {
    usort($group, function ($a, $b) {
        /* @a yii\rbac\Role */
        $children = Yii::$app->authManager->getChildRoles($a->name);
        return in_array($b->name, array_keys($children)) ? -1 : 1;
    });
}
unset($group);

$orderedRoles = [];
foreach ($groups as $group) {
    foreach ($group as $role) {
        $orderedRoles[] = $role;
    }
}

$roleItems = ArrayHelper::map($orderedRoles, 'name', 'description');
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-user',
    'enableClientValidation' => false
]); ?>

    <fieldset>
        <legend><?= Yii::t('azbuco.user', 'Account') ?></legend>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

    </fieldset>

    <fieldset>
        <legend><?= Yii::t('azbuco.user', 'Roles') ?></legend>

        <?=
        $form->field($model, 'roles')->checkboxList($roleItems, [
            'separator' => '',
            'item' => function ($index, $label, $name, $checked, $value) {

                $id = 'role-' . $index;
                $children = Yii::$app->authManager->getChildren($value);
                $permissions = [];
                foreach ($children as $child) {
                    if ($child->type == yii\rbac\Item::TYPE_PERMISSION) {
                        $permissions[] = $child;
                    }
                }

                $hasPermission = count($permissions) > 0 ? true : false;
                $hasPermission = false;

                $html = '<div class="custom-control custom-checkbox">';
                $html .= Html::checkbox($name, $checked, [
                    'id' => $id,
                    'class' => 'custom-control-input',
                    'value' => $value,
                    'data-children' => Json::encode(ArrayHelper::getColumn(Yii::$app->authManager->getChildRoles($value),
                        'name', false)),
                ]);

                $html .= Html::label($label, $id, [
                    'class' => 'custom-control-label',
                ]);

                if ($hasPermission) {
                    $html .= '<div class="list-icons">';
                    $html .= Html::a('', '#' . $id . '-permissions', [
                        'class' => 'list-icons-item',
                        'data-toggle' => 'collapse',
                        'data-action' => 'collapse',
                    ]);
                    $html .= '</div>';
                }

                $html .= '</div>';

                if ($hasPermission) {
                    $html .= '<div class="collapse" id="' . $id . '-permissions"><ul>';

                    foreach ($permissions as $permission) {
                        $text = $permission->description;
                        if (!empty($text)) {
                            $text = Yii::t('azbuco.survey', $text);
                        } else {
                            $text = $permission->name;
                        }
                        $html .= '<li>' . $text . '</li>';
                    }

                    $html .= '</ul></div>';
                }

                return $html;
            },
        ])->label(false);
        ?>

    </fieldset>

    <div class="form-buttons mb-0">
        <?= Html::submitButton(Yii::t('azbuco.user', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$this->registerJs(<<<JS

var setupRoles = function() {
    $('[name="UserForm[roles][]"]').each(function() {
        $(this).prop('disabled', false);
    });
    
    $('[name="UserForm[roles][]"]').each(function() {
        var t = $(this);
        if (t.is(':checked')) {
            var children = t.data('children');
            children.forEach(function(value) {
                var c = $('input[value="' + value + '"]');
                if (!c.is(t)) {
                    c.prop('disabled', true);
                    c.prop('checked', true);
                }
            });
        }
    });
};
    
setupRoles();
    
$('[name="UserForm[roles][]"]').on('change', function() {
    setupRoles();
});
    
JS
);
