<?php
/* @var $this yii\web\View */
/* @var $model azbuco\user\common\models\User */

$this->title = Yii::t('azbuco.user', 'Update User');
?>

<div style="max-width: 800px; margin: auto;">
    <div class="card">
        <div class="card-body">

            <?=
            $this->render('_form', [
                'model' => $model,
            ]);
            ?>

        </div>
    </div>
</div>