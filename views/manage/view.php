<?php

use azbuco\user\models\User;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $model User */

$this->title = $model->name;
?>

<p class="mb-2">
    <?= Html::a(Yii::t('azbuco.user', 'Back to all users'), ['/user/manage/index']) ?>
</p>

<div class="card">
    <div class="card-body">
        <?=
        DetailView::widget([
            'model' => $model,
            'options' => [
                'class' => 'table table-hover detail-view mb-3',
            ],
            'attributes' => [
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function ($m) {
                        /* @var $m User */
                        if ($m->deleted_at) {
                            return $m->name;
                        }
                        return '<div class="d-flex justify-content-between align-items-center"><div>'
                            . $m->name
                            . '</div><div>'
                            . Html::a(Yii::t('azbuco.user', 'Update User'), ['/user/manage/update', 'id' => $m->id], ['class' => 'btn btn-primary btn-sm d-block ml-3'])
                            . '</div></div>';
                    },
                ],
                [
                    'attribute' => 'accessTokens',
                    'format' => 'raw',
                    'value' => function ($m) {
                        /* @var $m User */
                        $output = '';
                        foreach ($m->accessTokens as $accessToken) {
                            $output .= '<div class="fw-bold">' . $accessToken->token . ' ' . Html::a(Yii::t('yii', 'Delete'), ['/user/manage/delete-access-token', 'id' => $accessToken->id]) . '</div>';
                            $output .= '<div class="small text-muted">' . $accessToken->name . '</div>';
                        }
                        return '<div class="d-flex justify-content-between align-items-center"><div>'
                            . $output
                            . '</div><div>'
                            . Html::a(Yii::t('azbuco.user', 'New Access Token'), ['/user/manage/create-access-token', 'id' => $m->id], ['class' => 'btn btn-primary btn-sm d-block ml-3'])
                            . '</div></div>';
                    }
                ],
                'email:email',
                [
                    'attribute' => 'role',
                    'label' => Yii::t('azbuco.user', 'Role'),
                    'format' => 'raw',
                    'value' => function ($m) {
                        /* @var $m User */
                        $roles = [];
                        foreach (Yii::$app->authManager->getRolesByUser($m->id) as $role) {
                            $roles[] = $role->name;
                        }
                        return implode(', ', $roles);
                    },
                ],
                'registration_ip',
                'blocked_at:datetime',
                'deleted_at:datetime',
                'updated_at:datetime',
                'created_at:datetime',
            ]
        ])
        ?>

        <div class="form-buttons">

            <?php if ($model->blocked_at && !$model->deleted_at): ?>

                <?=
                Html::a(Yii::t('azbuco.user', 'Unblock User'), ['/user/manage/block', 'id' => $model->id, 'block' => false], [
                    'class' => 'btn btn-primary',
                    'data' => [
                        'confirm' => Yii::t('azbuco.user', 'Are you sure you want to unblock this user?'),
                        'method' => 'POST',
                    ]
                ])
                ?>

            <?php elseif (!$model->deleted_at): ?>

                <?=
                Html::a(Yii::t('azbuco.user', 'Block User'), ['/user/manage/block', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('azbuco.user', 'Are you sure you want to block this user?'),
                        'method' => 'POST',
                    ]
                ])
                ?>

            <?php endif; ?>

            <?php if (!$model->deleted_at): ?>

                <?=
                Html::a(Yii::t('azbuco.user', 'Delete User'), ['/user/manage/delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('azbuco.user', 'Are you sure you want to delete this user?'),
                        'method' => 'POST',
                    ]
                ])
                ?>

            <?php endif; ?>

        </div>
    </div>
</div>
