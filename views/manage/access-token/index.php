<?php

use azbuco\user\models\User;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $dataProvider ArrayDataProvider */
/* @var $user User */

$this->title = Yii::t('azbuco.user', 'Access Tokens for {0}', [$user->name]);

$checked = '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 6c-3.313 0-6 2.687-6 6s2.687 6 6 6c3.314 0 6-2.687 6-6s-2.686-6-6-6z"/></svg>';
?>

<div class="d-flex justify-content-center w-100">
    <div class="col-md-10 col-lg-8 p-0">
        <div class="card my-5">
            <div class="card-header">
                <h5 class="mb-0">
                    <?= $this->title ?>
                </h5>
            </div>
            <div class="card-body">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'name',
                        ],
                        [
                            'attribute' => 'token',
                        ],
                        'created_at:datetime',
                        [
                            'label' => '',
                            'format' => 'raw',
                            'value' => function($m) {
                                /* @var $m \azbuco\user\models\AccessToken */
                                return Html::a(Yii::t('azbuco.user', 'Delete token'), ['/user/manage/access-token-delete', 'token' => $m->token], ['class' => 'btn btn-danger btn-sm', 'data-confirm' => Yii::t('azbuco.user', 'Are you sure you want to delete the token? External applications may require it.'), 'data-method' => 'post']);
                            },
                            'headerOptions' => [
                                'style' => 'width: 10px;'
                            ],
                            'contentOptions' => [
                                 'class' => 'text-nowrap',
                            ],
                        ],
                    ],
                ])
                ?>

                <div class="text-right mt-3">
                    <?= Html::a(Yii::t('azbuco.user', 'Create New Token'), ['/user/manage/create-access-token'], ['class' => 'btn btn-primary']) ?>
                </div>

            </div>

        </div>
    </div>
</div>
