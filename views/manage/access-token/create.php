<?php

use azbuco\user\models\AccessToken;
use azbuco\user\models\User;
use yii\web\View;

/* @var $this View */
/* @var $model AccessToken */
/* @var $user User */

$this->title = Yii::t('azbuco.user', 'Create Access Token for {0}', [$user->name]);
?>

<div class="card">
    <div class="card-header">
        <h5 class="mb-0">
            <?= $this->title ?>
        </h5>
    </div>
    <div class="card-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ]);
        ?>
    </div>
</div>

