<?php

use azbuco\user\models\AccessToken;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model AccessToken */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-access-token',
    'enableClientValidation' => false
]); ?>

<fieldset>
    <legend><?= Yii::t('azbuco.user', 'Access Token') ?></legend>

    <?= $form->field($model, 'token')->textInput(['maxlength' => true])->hint(Yii::t('azbuco.user', 'Token can be any string, but it must be unique.')) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->hint(Yii::t('azbuco.user', 'Optional name to identify the token later.')) ?>

</fieldset>

<div class="form-buttons mb-0">
    <?= Html::submitButton(Yii::t('azbuco.user', 'Save'), ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

