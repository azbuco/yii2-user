<?php

use azbuco\user\models\User;
use azbuco\user\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $searchModel UserSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = Yii::t('azbuco.user', 'All Users');

$checked = '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 6c-3.313 0-6 2.687-6 6s2.687 6 6 6c3.314 0 6-2.687 6-6s-2.686-6-6-6z"/></svg>';
?>

<p class="mb-2">
    <?= Html::a(Yii::t('azbuco.user', 'New User'), ['/user/manage/create'], ['class' => 'btn btn-primary']); ?>
</p>

<div class="card mb-5">
    <div class="card-header">
        <h5 class="mb-0">
            <?= $this->title ?>
        </h5>
    </div>
    <div class="card-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function ($m) {
                        /* @var $m User */
                        return Html::a($m->name, ['/user/manage/view', 'id' => $m->id]);
                    },
                    'contentOptions' => [
                        'class' => 'column-main',
                    ],
                ],
                [
                    'attribute' => 'email',
                ],
                [
                    'attribute' => 'role',
                    'label' => Yii::t('azbuco.user', 'Role'),
                    'format' => 'raw',
                    'value' => function ($m) {
                        /* @var $m User */
                        $roles = [];
                        foreach (Yii::$app->authManager->getRolesByUser($m->id) as $role) {
                            $roles[] = (!empty($role->description) ? $role->description : $role->name);
                        }
                        sort($roles);
                        return implode('<br />', $roles);
                    },
                    'filter' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', function ($item) {
                        return !empty($item->description) ? $item->description : $item->name;
                    }),
                ],
                [
                    'attribute' => 'confirmed_at',
                    'label' => Yii::t('azbuco.user', 'Confirmed'),
                    'format' => 'raw',
                    'value' => function ($m) use ($checked) {
                        /* @var $m User */
                        return $m->confirmed_at ? $checked : '';
                    },
                    'visible' => $this->context->module->requireEmailConfirmation,
                    'contentOptions' => [
                        'class' => 'text-center',
                    ],
                    'filter' => [
                        'true' => Yii::t('azbuco.user', 'Confirmed'),
                        'false' => Yii::t('azbuco.user', 'Unconfirmed'),
                    ]
                ],
                [
                    'attribute' => 'blocked_at',
                    'label' => Yii::t('azbuco.user', 'Blocked'),
                    'format' => 'raw',
                    'value' => function ($m) use ($checked) {
                        /* @var $m User */
                        return $m->blocked_at ? $checked : '';
                    },
                    'contentOptions' => [
                        'class' => 'text-center',
                    ],
                    'filter' => [
                        'true' => Yii::t('azbuco.user', 'Blocked'),
                        'false' => Yii::t('azbuco.user', 'Allowed'),
                    ]
                ],
                [
                    'attribute' => 'deleted_at',
                    'label' => Yii::t('azbuco.user', 'Deleted'),
                    'format' => 'raw',
                    'value' => function ($m) use ($checked) {
                        /* @var $m User */
                        return $m->deleted_at ? $checked : '';
                    },
                    'contentOptions' => [
                        'class' => 'text-center',
                    ],
                    'filter' => [
                        'true' => Yii::t('azbuco.user', 'Deleted'),
                        'false' => Yii::t('azbuco.user', 'Active'),
                    ]
                ],
                [
                    'attribute' => 'accessToken',
                    'label' => Yii::t('azbuco.user', 'Access Tokens'),
                    'format' => 'raw',
                    'value' => function ($m) use ($checked) {
                        /* @var $m User */
                        return count($m->accessTokens) > 0 ? $checked : '';
                    },
                    'contentOptions' => [
                        'class' => 'text-center',
                    ],
                ],
                'updated_at:datetime',
                'created_at:datetime',
            ],
        ])
        ?>
    </div>
</div>
