<?php
/* @var $this yii\web\View */
/* @var $model azbuco\user\common\models\User */

$this->title = Yii::t('azbuco.user', 'Crete User');
?>

<div class="card my-5">
    <div class="card-header">
        <h5 class="mb-0">
            <?= $this->title ?>
        </h5>
    </div>
    <div class="card-body">

        <?=
        $this->render('_form', [
            'model' => $model,
        ]);
        ?>

    </div>
</div>
