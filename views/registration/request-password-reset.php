<?php

use azbuco\user\models\RequestPasswordResetForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model RequestPasswordResetForm */
/* @var $isMailSent boolean */

$this->title = Yii::t('azbuco.user', 'Request Password Reset');
?>

<div class="d-flex justify-content-center w-100">
    <div class="col-md-6 col-lg-4 p-0">
        <div class="card my-5">
            <div class="card-body">

                <h5 class="text-center mb-3">
                    <?= $this->title ?>
                </h5>

                <?php if ($isMailSent): ?>
                    <p>
                        <?= Yii::t('azbuco.user', 'We have sent you an email with reset instructions.') ?>
                    </p>
                    <p>
                        <?= Yii::t('azbuco.user', 'If the email does not arrive soon, check your spam folder.') ?>
                    </p>

                <?php else: ?>

                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'form-request-password-reset',
                        'enableClientValidation' => false,
                    ]);
                    ?>

                    <?=
                    $form->field($model, 'email', [
                        'errorOptions' => [
                            'encode' => false,
                            'class' => 'help-block'
                        ],
                    ])->textInput()
                    ?>

                    <p class="text-muted">
                        <?= Yii::t('azbuco.user', 'To reset your password, enter your email address and click continue.') ?>
                    </p>

                    <div class="form-group text-center mb-0">
                        <?=
                        Html::submitButton(Yii::t('azbuco.user', 'Continue'), [
                            'class' => 'btn btn-primary mb-0',
                        ])
                        ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                <?php endif ?>

            </div>
        </div>
    </div>
</div>
