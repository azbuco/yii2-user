<?php

use azbuco\user\models\User;
use yii\web\View;

/* @var $this View */
/* @var $model User */
/* @var $redirect string */

$this->title = Yii::t('azbuco.user', 'Successfull email confirmation.');
?>

<div class="content d-flex justify-content-center w-100">
    <div class="my-5">
        <h3><?= Yii::t('azbuco.user', 'Sign Up Complete.') ?></h3>
        <p>
            <?= Yii::t('azbuco.user', 'This page is redirecting in 5 seconds...') ?>
        </p>
    </div>
</div>

<?php
$this->registerJs(<<<JS

window.setTimeout(function(){
    window.location.href = "$redirect";
}, 5000);

JS
);