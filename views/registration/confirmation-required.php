<?php

use azbuco\user\models\User;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $model User|null */

$this->title = Yii::t('azbuco.user', 'Please activate your account');
?>

<div class="content d-flex justify-content-center w-100">
    <div class="card my-5">
        <div class="card-body text-center">

            <h5 class="mb-3"><?= Yii::t('azbuco.user', 'Hello {name}!', ['name' => $model->name]) ?></h5>

            <p>
                <?= Yii::t('azbuco.user', 'We have sent you a confirmation message to <b>{email}.</b>', ['email' => $model->email]) ?>
            </p>

            <p>
                <?= Yii::t('azbuco.user', 'Please check your inbox, and click the link in the email to confirm your email address.') ?>
            </p>

            <p>
                <?=
                Yii::t('azbuco.user', '<a href="{url}">Resend confirmation email.</a>', [
                    'url' => Url::to(['/user/registration/request-confirm-resend', 'token' => $model->email_confirmation_token], true)
                ])
                ?>
            </p>
        </div>
    </div>
</div>
