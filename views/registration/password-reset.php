<?php

use azbuco\user\models\PasswordResetForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model PasswordResetForm */

$this->title = Yii::t('azbuco.user', 'Password Reset');
?>

<div class="d-flex justify-content-center w-100">
    <div class="col-md-6 col-lg-4 p-0">
        <div class="card my-5">
            <div class="card-body">
                
                <h5 class="text-center mb-3">
                    <?= $this->title ?>
                </h5>

                <?php $form = ActiveForm::begin(['id' => 'form-password-reset']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true])->hint(Yii::t('azbuco.user', 'Enter your new password.')) ?>

                <div class="form-group text-center mb-0">
                    <?=
                    Html::submitButton(Yii::t('azbuco.user', 'Save New Password'), [
                        'class' => 'btn btn-primary mb-0',
                    ])
                    ?>
                </div>
                <?php ActiveForm::end(); ?>


            </div>
        </div>
    </div>
</div>
