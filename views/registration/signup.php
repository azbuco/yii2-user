<?php

use azbuco\user\models\SignupForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $model SignupForm */

$this->title = Yii::t('azbuco.user', 'Sign Up');
?>

<div class="d-flex justify-content-center w-100">
    <div class="col-md-6 col-lg-4 p-0">
        <div class="card my-5">
            <div class="card-body">

                <h5 class="text-center mb-3">
                    <?= $this->title ?>
                </h5>

                <?php
                $form = ActiveForm::begin([
                    'id' => 'form-signup',
                    'enableClientValidation' => false,
                ]);
                ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?=
                $form->field($model, 'email', [
                    'errorOptions' => [
                        'encode' => false,
                        'class' => 'help-block'
                    ],
                ])->textInput()
                ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php if ($model->hasAccept()): ?>

                    <?= $form->field($model, 'accept')->checkbox() ?>

                <?php endif; ?>

                <div class="form-group text-center mb-0">
                    <?=
                    Html::submitButton(Yii::t('azbuco.user', 'Sign Up'), [
                        'class' => 'btn btn-primary mb-0',
                    ])
                    ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

