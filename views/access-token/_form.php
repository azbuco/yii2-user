<?php

use azbuco\user\models\AccessToken;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model AccessToken */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'form-access-token']); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true])->hint(Yii::t('azbuco.user', 'Provide a short description for the token.')) ?>

<div class="form-buttons mb-0 mt-3">
    <?= Html::submitButton(Yii::t('azbuco.user', 'Save'), ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


