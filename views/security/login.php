<?php
/* @var $this View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

use common\models\LoginForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = Yii::t('azbuco.user', 'Sign In');

$this->params['breadcrumbs'] = [
    $this->title,
]
?>

<div class="d-flex justify-content-center w-100">
    <div class="col-md-6 col-lg-4 p-0">
        <div class="card mt-5 mb-3">
            <div class="card-body">

                <h5 class="text-center mb-3">
                    <?= Yii::t('azbuco.user', 'Sign In') ?>
                </h5>

                <?php $form = ActiveForm::begin(['id' => 'form-login']); ?>

                <?=
                $form->field($model, 'email', [
                    'errorOptions' => [
                        'encode' => false,
                        'class' => 'help-block'
                    ],
                ])->textInput()
                ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group text-center">
                    <?= Html::submitButton(Yii::t('azbuco.user', 'Sign In'), ['class' => 'btn btn-primary mb-0']) ?>


                </div>

                <p class="text-center mb-0">
                    <?= Html::a(Yii::t('azbuco.user', 'Forgot password?'), ['/user/registration/request-password-reset']) ?>
                </p>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

        <?php if ($model->module->enableRegistration): ?>
            <p class="text-center">
                <?=
                Yii::t('azbuco.user', 'Not a member? <a href="{url}">Sign Up!</a>', [
                    'url' => Url::to(['/user/registration/signup'])
                ])
                ?>
            </p>
        <?php endif; ?>
    </div>
</div>
