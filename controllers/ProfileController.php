<?php

namespace azbuco\user\controllers;

use azbuco\user\models\EmailChangeForm;
use azbuco\user\models\PasswordChangeForm;
use Yii;
use yii\filters\AccessControl;
use yii\gii\Module;
use yii\helpers\Url;
use yii\web\Controller;
use yiiunit\extensions\bootstrap4\data\User;

/**
 * User profile related actions
 * 
 * @property Module $module
 */
class ProfileController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionPasswordChange()
    {
        $model = new PasswordChangeForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('azbuco.user', 'New password is saved. Try to sign in with your new password.'));
            Yii::$app->user->logout();
            return $this->redirect(['/user/security/login']);
        }

        return $this->render('password-change', [
                'model' => $model,
        ]);
    }

    public function actionEmailChange()
    {
        $model = new EmailChangeForm();
        if ($model->load(Yii::$app->request->post()) && ($user = $model->save())) {
            if (!$this->module->requireEmailConfirmation) {
                Yii::$app->getSession()->setFlash('success', Yii::t('azbuco.user', 'Successfull email change.'));
                Yii::$app->user->logout();
                return $this->redirect(['/user/security/login']);
            }

            if ($this->sendConfirmationEmail($user)) {
                return $this->redirect($this->resolveUrl($this->module->confirmationPage, $user));
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('azbuco.user', 'Unable to send verification email. Please check your email address.'));
            }
        }

        return $this->render('email-change', [
                'model' => $model,
        ]);
    }

    /**
     * 
     * @param User $user
     * @return boolean Whether the email sent or not.
     */
    protected function sendConfirmationEmail($user)
    {
        $views = [
            'html' => '@azbuco/user/mail/email-change-confirmation-html',
            'text' => '@azbuco/user/mail/email-change-confirmation-text'
        ];

        $params = [
            'user' => $user,
            'activateUrl' => Url::to([
                '/user/registration/confirmation',
                'token' => $user->email_confirmation_token,
                ], true),
        ];

        $success = Yii::$app->mailer->compose($views, $params)
            ->setFrom([$this->module->fromEmail => $this->module->fromName])
            ->setTo($user->email)
            ->setSubject($this->module->confirmationSubject)
            ->send();

        if ($success) {
            Yii::$app->getSession()->setFlash('success', Yii::t('azbuco.user', 'Confirmation email sent.'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('azbuco.user', 'Unable to send confirmation email.'));
        }

        return $success;
    }

}
