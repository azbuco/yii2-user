<?php

namespace azbuco\user\controllers;

use azbuco\user\models\AccessToken;
use azbuco\user\models\User;
use azbuco\user\models\UserForm;
use azbuco\user\models\UserSearch;
use azbuco\user\UserModule;
use Yii;
use yii\base\Exception;
use yii\data\ArrayDataProvider;
use yii\db\IntegrityException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * User management related actions
 *
 * @property UserModule $module
 */
class ManageController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        if (!empty($this->module->accessRules)) {
            $rules = $this->module->accessRules;
        } else {
            $rules = [
                [
                    'allow' => true,
                    'roles' => $this->module->accessRoles,
                ],
            ];
        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-access-token' => ['POST'],
                    'block' => ['POST'],
                ],
            ],
        ];
    }

    public function actionTest()
    {
        $this->sendUserNotificationEmail('sztamas@realszisztema.hu', 'Székely Tamás ' . date('H:i:s'), 'abc123');
    }

    protected function sendUserNotificationEmail($email, $name, $password)
    {
        $views = [
            'html' => '@azbuco/user/mail/user-notification-html',
            'text' => '@azbuco/user/mail/user-notification-text'
        ];

        $params = [
            'email' => $email,
            'name' => $name,
            'password' => $password,
            'baseUrl' => Url::base(true),
        ];

        $success = Yii::$app->mailer->compose($views, $params)
            ->setFrom([$this->module->fromEmail => $this->module->fromName])
            ->setTo($email)
            ->setSubject($this->module->notificationSubject)
            ->send();

        if ($success) {
            Yii::$app->getSession()->setFlash('success', Yii::t('azbuco.user', 'Notification email sent.'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('azbuco.user', 'Unable to send notification email.'));
        }

        return $success;
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = Yii::createObject(UserForm::class, [null, $this->module]);
        $model->password = Yii::$app->security->generateRandomString(16);
        $model->scenario = UserForm::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post()) && $model->create()) {
            if ($this->module->sendUserNotification) {
                $this->sendUserNotificationEmail($model->email, $model->name, $model->password);
            }
            return $this->redirect(['/user/manage/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $model = Yii::createObject(UserForm::class, [$user, $this->module]);
        $model->scenario = UserForm::SCENARIO_UPDATE;

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            return $this->redirect(['/user/manage/index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionBlock($id, $block = true)
    {
        if ($block) {
            $this->findModel($id)->touch('blocked_at');
        } else {
            $this->findModel($id)->updateAttributes(['blocked_at' => null]);
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->statusCode = 202;
            return;
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($this->module->softDeleteUser) {
            $model->touch('deleted_at');
            $model->updateAttributes([
                'email' => 'deleted.' . Yii::$app->security->generateRandomString() . ($this->module->anonymizeUserOnDelete ? '' : '.' . $model->email),
                'name' => $this->module->anonymizeUserOnDelete ? Yii::t('azbuco.user', 'Deleted User') : $model->name,
            ]);
        } else {
            try {
                $model->delete();
            } catch (IntegrityException $ex) {
                Yii::$app->session->setFlash('error', Yii::t('azbuco.user', 'Unable to delete user. Please remove all user related data from the database.'));
                return $this->redirect(['/user/manage/view', 'id' => $model->id]);
            }
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->statusCode = 202;
            return;
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Creates a new AccessToken model.
     * @return mixed
     * @throws Exception
     */
    public function actionCreateAccessToken($id)
    {
        $user = $this->findModel($id);

        $model = new AccessToken();
        $model->user_id = $user->id;
        $model->token = Yii::$app->security->generateRandomString();
        $model->name = Yii::t('azbuco.user', 'Generated at {datetime}', ['datetime' => Yii::$app->formatter->asDatetime(time())]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/user/manage/view', 'id' => $user->id]);
        }

        return $this->render('access-token/create', [
            'model' => $model,
            'user' => $user,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDeleteAccessToken($id)
    {
        $model = $this->findAccessToken($id);

        $model->delete();

        $this->redirect(['/user/manage/view', 'id' => $model->user_id]);
    }

    /**
     * Finds the AccessToken model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AccessToken the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findAccessToken($id)
    {
        if (($model = AccessToken::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
