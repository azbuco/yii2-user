<?php

namespace azbuco\user\controllers;

use azbuco\user\models\PasswordResetForm;
use azbuco\user\models\RequestPasswordResetForm;
use azbuco\user\models\SignupForm;
use azbuco\user\models\User;
use azbuco\user\UserModule;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * All the registration related actions
 * 
 * @property UserModule $module
 */
class RegistrationController extends Controller {
 
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->checkRegistrationEnabled();

        $model = new SignupForm($this->module);

        if ($model->load(Yii::$app->request->post())) {
            $user = $model->signup();
            if ($user) {
                if (!$this->module->requireEmailConfirmation) {
                    if ($this->module->autoLoginAfterRegistration) {
                        Yii::$app->user->login($user, 0);
                        return $this->goBack();
                    }
                    Yii::$app->getSession()->setFlash('success', Yii::t('azbuco.user', 'Successfull signup.'));
                    return $this->redirect($this->resolveUrl($this->module->redirectAfterSignup, $user));
                }

                if ($this->sendConfirmationEmail($user)) {
                    return $this->redirect($this->resolveUrl($this->module->confirmationPage, $user));
                } else {
                    Yii::$app->getSession()->setFlash('error', Yii::t('azbuco.user', 'Unable to send verification email. Please check your email address.'));
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionConfirmationRequired($token)
    {
        $model = User::findByEmailConfirmationToken($token);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if ($model->confirmed_at) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('confirmation-required', [
            'model' => $model,
        ]);
    }

    /**
     * Confirm registration process
     */
    public function actionConfirmation($token)
    {
        $this->checkRegistrationEnabled();

        $model = User::findByEmailConfirmationToken($token);
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($model->confirmed_at) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model->touch('confirmed_at');
        Yii::$app->getSession()->setFlash('success', Yii::t('azbuco.user', 'Successfull email confirmation.'));

        $redirect = $this->resolveUrl($this->module->redirectAfterSignup, $model);

        if ($this->module->autoLoginAfterRegistration) {
            Yii::$app->getUser()->login($model);
            $redirect = Yii::$app->getUser()->getReturnUrl(Url::to($redirect));
        }

        return $this->render('confirmation', [
            'redirect' => $redirect,
            'model' => $model,
        ]);
    }

    /**
     * Requests a confrimatin resend.
     * 
     * @return mixed
     */
    public function actionRequestConfirmResend($token)
    {
        $this->checkRegistrationEnabled();

        $user = User::findByEmailConfirmationToken($token);
        if ($user === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($this->sendConfirmationEmail($user)) {
            return $this->redirect($this->resolveUrl($this->module->confirmationPage, $user));
        }

        throw new ServerErrorHttpException('Unable to send email.');
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new RequestPasswordResetForm($this->module->requireEmailConfirmation);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::findByEmail($model->email);
            $success = $this->sendPasswordResetEmail($user);

            if ($success) {
                Yii::$app->getSession()->setFlash('success', Yii::t('azbuco.user', 'Password request email sent.'));
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('azbuco.user', 'Unable to send password request email.'));
            }
        }

        return $this->render('request-password-reset', [
            'model' => $model,
            'isMailSent' => $success ?? false,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionPasswordReset($token)
    {
        try {
            $model = new PasswordResetForm($this->module, $token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('azbuco.user', 'New password is saved.'));
            return $this->redirect(['/user/security/login']);
        }

        return $this->render('password-reset', [
            'model' => $model,
        ]);
    }

    public function actionPasswordRequired($token) {
        try {
            $model = new PasswordRequireForm($this->module, $token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('azbuco.user', 'New password is saved.'));
            return $this->redirect(['/user/security/login']);
        }

        return $this->render('password-required', [
            'model' => $model,
        ]);
    }

    /**
     * 
     * @param User $user
     * @return boolean Whether the email sent or not.
     * @throws ServerErrorHttpException
     */
    protected function sendPasswordResetEmail($user)
    {
        if (!User::isTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                throw new ServerErrorHttpException('Error while saving user data.');
            }
        }

        $views = [
            'html' => '@azbuco/user/mail/password-reset-html',
            'text' => '@azbuco/user/mail/password-reset-text'
        ];

        $params = [
            'user' => $user,
            'resetUrl' => Url::to([
                '/user/registration/password-reset',
                'token' => $user->password_reset_token
            ], true),
        ];

        $success = Yii::$app->mailer->compose($views, $params)
        ->setFrom([$this->module->fromEmail => $this->module->fromName])
        ->setTo($user->email)
        ->setSubject($this->module->confirmationSubject)
        ->send();

        if ($success) {
            Yii::$app->getSession()->setFlash('success', Yii::t('azbuco.user', 'Password reset email sent.'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('azbuco.user', 'Unable to send password reset email.'));
        }

        return $success;
    }

    /**
     * 
     * @param User $user
     * @return boolean Whether the email sent or not.
     */
    protected function sendConfirmationEmail($user)
    {
        $views = [
            'html' => '@azbuco/user/mail/signup-confirmation-html',
            'text' => '@azbuco/user/mail/signup-confirmation-text'
        ];

        $params = [
            'user' => $user,
            'activateUrl' => Url::to([
                '/user/registration/confirmation',
                'token' => $user->email_confirmation_token,
            ], true),
        ];

        $success = Yii::$app->mailer->compose($views, $params)
        ->setFrom([$this->module->fromEmail => $this->module->fromName])
        ->setTo($user->email)
        ->setSubject($this->module->confirmationSubject)
        ->send();

        if ($success) {
            Yii::$app->getSession()->setFlash('success', Yii::t('azbuco.user', 'Confirmation email sent.'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('azbuco.user', 'Unable to send confirmation email.'));
        }

        return $success;
    }

    /**
     * If the registration disabled, shows 404 error page
     * @throws NotFoundHttpException
     */
    protected function checkRegistrationEnabled()
    {
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Resolve URLs 
     * @param mixed $redirect string, url definition array, or an anonymous function with the signature: function($user, $module) {}
     * @param type $user
     * @return type
     */
    protected function resolveUrl($redirect, $user)
    {
        if (is_array($redirect) || is_string($redirect)) {
            return Url::to($redirect);
        }

        return call_user_func($redirect, $user, $this->module);
    }

}
