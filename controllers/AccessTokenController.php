<?php

namespace azbuco\user\controllers;

use azbuco\user\models\AccessToken;
use azbuco\user\UserModule;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;

/**
 * User access token related actions
 *
 * @property UserModule $module
 */
class AccessTokenController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AccessToken models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => AccessToken::find()
                ->where(['user_id' => Yii::$app->user->id])
                ->orderBy([
                    'created_at' => SORT_ASC,
                ])
                ->all(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new AccessToken model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AccessToken();
        $model->user_id = Yii::$app->user->id;
        $model->token = Yii::$app->security->generateRandomString();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/user/access-token/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($token)
    {
        $model = $this->findModel($token);

        if ($model->user_id !== Yii::$app->user->id) {
            throw new UnauthorizedHttpException('You are not authorized to delete this token.');
        }

        $model->delete();

        $this->redirect(['/user/access-token/index']);
    }

    /**
     * Finds the AccessToken model based on its token.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $token
     * @return AccessToken the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($token)
    {
        $model = AccessToken::find()->where(['token' => $token])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
