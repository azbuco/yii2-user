<?php

namespace azbuco\user\migrations;

use yii\db\Migration;

/**
 * Add user_options EAV table
 */
class m200428_072800_user_options extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
        '{{%user_options}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'key' => $this->string(),
            'int_value' => $this->integer(),
            'num_value' => $this->decimal(18, 6),
            'string_value' => $this->string(255),
            'text_value' => $this->text(),
            'json_value' => $this->text(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-user_options-user_id', '{{%user_options}}', ['user_id', 'key'], true);
        $this->addForeignKey('fk-user_options-user_id', '{{%user_options}}', ['user_id'], '{{%user}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_options}}');
    }

}
