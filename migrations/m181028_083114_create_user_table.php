<?php

namespace azbuco\user\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m181028_083114_create_user_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        if ($this->db->getTableSchema('{{%user}}') !== null) {
            $this->renameTable('{{%user}}', '{{%user_' . time() . '}}');
        }

        $this->createTable(
        '{{%user}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull()->unique(),
            'password_hash' => $this->string(60)->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_reset_token' => $this->string(255)->unique(),
            'email_confirmation_token' => $this->string(255)->notNull()->unique(),
            'registration_ip' => $this->string(45),
            'confirmed_at' => $this->integer(),
            'deleted_at' => $this->integer(),
            'blocked_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx_user_email', '{{%user}}', ['email', 'password_hash', 'auth_key'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }

}
