<?php

namespace azbuco\user\migrations;

class m200914_105500_add_key_to_access_token extends \yii\db\Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%access_token}}', 'key', $this->string(255)->after('name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%access_token}}', 'key');
    }

}