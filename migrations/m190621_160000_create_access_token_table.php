<?php

namespace azbuco\user\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `access_token`.
 */
class m190621_160000_create_access_token_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
        '{{%access_token}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'token' => $this->string(255)->notNull(),
            'name' => $this->string(255),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-access_token-token', '{{%access_token}}', ['token'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%access_token}}');
    }

}
