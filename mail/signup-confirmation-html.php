<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user azbuco\user\models\User */
/* @var $activateUrl string */
?>

<p>Hello <?= Html::encode($user->name) ?>,</p>

<p>Follow the link below to activate your subscription:</p>

<p><?= Html::a(Html::encode($activateUrl), $activateUrl) ?></p>