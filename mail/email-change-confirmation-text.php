<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user azbuco\user\models\User */
/* @var $activateUrl string */
?>

Hello <?= Html::encode($user->name) ?>,

Follow the link below to activate your subscription:

<?= $activateUrl ?>