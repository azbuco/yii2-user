<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $email string */
/* @var $password string */
/* @var $baseUrl string */
?>

Hello <?php= echo $name ?>,

This is a notification email about your registration.
Please use the password "<?= $password ?>" to login
at <?= $baseUrl ?>