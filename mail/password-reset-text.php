<?php
/* @var $this yii\web\View */
/* @var $user common\models\base\User */
/* @var $resetUrl string */
?>
Hello <?= $user->name ?>,

Follow the link below to reset your password:

<?= $resetUrl ?>
