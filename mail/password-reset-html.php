<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\base\User */
/* @var $resetUrl string */

?>
<p>Hello <?= Html::encode($user->name) ?>,</p>

<p>Follow the link below to reset your password:</p>

<p><?= Html::a(Html::encode($resetUrl), $resetUrl) ?></p>

