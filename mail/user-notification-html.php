<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $email string */
/* @var $password string */
/* @var $baseUrl string */
?>

<p>Hello <?= Html::encode($name) ?>,</p>

<p>This is a notification email about your registration.<br />
    Please use the password <b><?= Html::encode($password) ?></b> to login at<br />
    <?= Html::a(Html::encode($baseUrl), $baseUrl) ?>
<p>