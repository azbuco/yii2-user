# Yii2 User Management Module #

## Install ##

Install with composer

Remove the default user migration. Dont forget to backup your data. 
If you have existing users, you have to migrate them manually later.

In the composer.json file add these sections: 

```
"require": {
    // ..
    "azbuco/yii2-user": "dev-master",
    // ..
}
"repositories": [
    // ..
    {
        "type": "vcs",
        "url": "https://azbuco@bitbucket.org/azbuco/yii2-user.git"
    },
    {
        "type": "vcs",
        "url": "https://azbuco@bitbucket.org/azbuco/yii2-eav.git"
    },
    // ..
]
```

### Configure authmanager component

Set the authmanager and user components in your main config file 
(probably common\config\main.php):

```
'components' => [
    // ...
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
        // uncomment if you want to cache RBAC items hierarchy
        // 'cache' => 'cache',
    ],
    // ...
],
```

### Migrations

Add the default rbac migrations to console config:

```
'controllerMap' => [
    'migrate' => [
        'class' => \yii\console\controllers\MigrateController::class,
        'migrationPath' => [
            '@console/migrations',      // default migrations
            '@yii/rbac/migrations',     // yii rbac migrations
        ],
        'migrationNamespaces' => [
            'azbuco\user\migrations',   // migrations for this module
        ],
    ],
],
```

Remove the existing user migrations from the console folder.

Apply the mirgrations from the console, and check the database. If you have some 
errors, its probably because you don't deleted the default user migration.

### Config settings

In the main config file update the user component:

```
'components' => [
    // ...
    'user' => [
        'class' => 'yii\web\User',
        'identityClass' => azbuco\user\models\User::class,
        'enableAutoLogin' => true,
        'loginUrl' => ['/user/security/login'],
        // for configuration see the azbuco\user\models\User class,
    ],
    // ...
],
```

When you use the advanced app template, remove the redundant sections from the backend
and frontend config.

Configure the user module still in the main config file:

```
'modules' => [
    // ...
    'user' => [
        'class' => azbuco\user\UserModule::class,
        'requireEmailConfirmation' => false,
        'enableRegistration' => false,
        // for configuration see the azbuco\user\Module class,
    ],
    // ...
],
```

If you want to use the console interface, add the bootstrapping to the console config file:

```
'bootstrap' => [
    // ...,
    function() {
        // there is a component called user, so we specify the user module
        return Yii::$app->getModule('user');
    },
],
```

### Before first use

Run the migrations.

From the console create your first user, and promote it to the first role.

## Tests ##

Before running the acceptance tests, see the tests/acceptance.suite.yml file comments.

