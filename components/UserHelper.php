<?php

namespace azbuco\user\components;

use Yii;

class UserHelper
{
    public static function findUserIdsByRole($roleName)
    {
        $allRoles = Yii::$app->authManager->getRoles();
        $roles = [];
        foreach ($allRoles as $role) {
            $childRoles = Yii::$app->authManager->getChildRoles($role->name);
            foreach ($childRoles as $childRole) {
                if ($childRole->name === $roleName) {
                    $roles[] = $role->name;
                }
            }
        }

        $userIds = [];
        foreach ($roles as $role) {
            $ids = Yii::$app->authManager->getUserIdsByRole($role);
            $userIds = array_merge($userIds, $ids);
        }

        return $userIds;
    }
}
