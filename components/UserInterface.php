<?php

namespace azbuco\user\components;

interface UserInterface
{

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email);

    public static function findByPasswordResetToken($token);
    
}
