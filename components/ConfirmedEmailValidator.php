<?php

namespace azbuco\user\components;

use azbuco\user\models\User;
use Yii;
use yii\helpers\Url;
use yii\validators\Validator;

class ConfirmedEmailValidator extends Validator {

    public $confirmedAtProperty = 'confirmed_at';
    
    public function validateAttribute($model, $attribute)
    {        
        $user = User::findByEmail($model->$attribute);
        if ($user === null) {
            return;
        }

        if ($user->{$this->confirmedAtProperty} === null) {
            $message = Yii::t('azbuco.user', 'Not verified user.');
            $message .= ' ';
            $message .= Yii::t('azbuco.user', '<a href="{url}">Resend confirmation email.</a>', [
                'url' => Url::to(['/user/registration/request-confirm-resend', 'token' => $user->email_confirmation_token], true)
            ]);
            $this->addError($model, $attribute, $message);
        }
    }

}
