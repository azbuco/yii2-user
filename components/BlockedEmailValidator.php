<?php

namespace azbuco\user\components;

use azbuco\user\models\User;
use Yii;
use yii\validators\Validator;

class BlockedEmailValidator extends Validator {

    public $blockedAtProperty = 'blocked_at';
    
    public function validateAttribute($model, $attribute)
    {        
        $user = User::findByEmail($model->$attribute);
        if ($user === null) {
            return;
        }

        if ($user->{$this->blockedAtProperty} !== null) {
            $message = Yii::t('azbuco.user', 'Blocked email address. For further information, please contact an administrator.');
            $this->addError($model, $attribute, $message);
        }
    }

}
