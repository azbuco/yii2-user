<?php

namespace azbuco\user;

use azbuco\user\models\User;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\Module;
use yii\console\Application;
use yii\helpers\Url;
use yii\i18n\PhpMessageSource;

/**
 * User module definition class.
 */
class UserModule extends Module implements BootstrapInterface {

    /**
     * @var array Config for accesRules on the manage pages.
     */
    public $accessRules = [];

    /**
     * @var array Array of roles who can manage users.
     * Ist only has effect when the accesRule property is null.
     */
    public $accessRoles = ['superuser'];

    /**
     * @var string If requireAcceptTerms is true, the form automatically gets a checkbox with this label.
     * The default implementation (see the init method) lacks the ability of linking.
     */
    public $acceptTermsLabel;

    /**
     * @var string Default error if the acceptTerms not checked.
     */
    public $acceptTermsError;
    
    /**
     * @var On soft delete, the user data can be anonymized (GDPR)
     */
    public $anonymizeUserOnDelete = true;

    /**
     * @var boolean If true, automatically signs in the user after successfull registration, and redirects to the last visited page.
     */
    public $autoLoginAfterRegistration = true;

    /**
     * @var mixed Confirmation page URL
     * Can be an array or string, and the URL will be calculated with the yii\helpers\Url::to() method
     * or a closure with signature: function($user, $module) { return URL; }
     * See the default implementation in the init method.
     */
    public $confirmationPage;

    /**
     * @var string Subject for the confirmation email.
     */
    public $confirmationSubject = null;

    /**
     * @var bool If true, the user email can be any string. In this case the email functions will not work. Use with caution!
     */
    public $enableInvalidEmail = false;

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'azbuco\user\controllers';

    /**
     *
     * @var boolean It true, the module provides some flash messages during the signup.
     */
    public $enableFlashes = true;

    /**
     *
     * @var boolean If false, the registration pages return 404
     */
    public $enableRegistration = true;

    /**
     * @var string Sending emails from this email address.
     * If null, the no-reply@domain email will be used.
     */
    public $fromEmail = null;

    /**
     * @var string Sending emails with this name as sender.
     * If null, the application name.
     */
    public $fromName = null;

    /**
     * @var mixed Where to redirect after signup, when no email confirmation required.
     * Can be an array or string, and the URL will be calculated with the yii\helpers\Url::to() method
     * or a closure with signature: function($user, $module) { return URL; }
     */
    public $redirectAfterSignup = ['/user/security/login'];

    /**
     * @var integer Minimum length of accepted password during signup or password reset.
     */
    public $passwordMinLength = 6;

    /**
     * @var string Subject for the password reset email.
     */
    public $passwordResetSubject;

    /**
     * @var callable A callable for complex password validation (ex: require non alphanumeric character)
     */
    public $passwordValidator;

    /**
     * @var boolean If true, the accept terms checkbox automatically added to the signup form
     */
    public $requireAcceptTerms = false;

    /**
     * @var boolean If true, the module will send an email with a confirmation 
     * link that user needs to click through to complete its registration process.
     */
    public $requireEmailConfirmation = true;

    /**
     * @var bool If true, when a user created on the admin interface, the module sends a notificiation email
     * to the registered email address.
     */
    public $sendUserNotification = true;

    /**
     * @var string notification email subject
     */
    public $notificationSubject;

    /**
     * @var boolean If true, the user delete won't remove the user from the database, only marks it deleted.
     */
    public $softDeleteUser = true;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->registerTranslations();

        if ($this->acceptTermsLabel === null) {
            $this->acceptTermsLabel = Yii::t('azbuco.user', 'I agree to accept the Terms and Conditions');
        }

        if ($this->redirectAfterSignup === null) {
            $this->redirectAfterSignup = ['user/security/login'];
        }

        if ($this->fromEmail === null) {
            $this->fromEmail = 'no-reply@' . ($_SERVER['SERVER_NAME'] ?? 'example.com');
        }

        if ($this->fromName === null) {
            $this->fromName = Yii::$app->name;
        }

        if ($this->confirmationSubject === null) {
            $this->confirmationSubject = Yii::t('azbuco.user', 'Confirm account on {appname}', ['appname' => Yii::$app->name]);
        }

        if ($this->confirmationSubject === null) {
            $this->confirmationSubject = Yii::t('azbuco.user', 'Reset password on {appname}', ['appname' => Yii::$app->name]);
        }

        if ($this->confirmationPage === null) {
            $this->confirmationPage = function($user) {
                /* @var $user User */
                return Url::to(['/user/registration/confirmation-required', 'token' => $user->email_confirmation_token]);
            };
        }

        if ($this->notificationSubject === null) {
            $this->notificationSubject = Yii::t('azbuco.user', 'New registration at {appname}', ['appname' => Yii::$app->name]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $this->controllerNamespace = 'azbuco\user\commands';
        }
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->get('i18n')->translations['azbuco.user*'])) {
            Yii::$app->get('i18n')->translations['azbuco.user*'] = [
                'class' => PhpMessageSource::class,
                'basePath' => '@azbuco/user/messages',
                'sourceLanguage' => 'en-US',
//                'fileMap' => [
//                    'azbuco.user' => 'user.php',
//                ]
            ];
        }
    }

}
