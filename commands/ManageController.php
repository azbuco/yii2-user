<?php

namespace azbuco\user\commands;

use azbuco\user\models\AccessToken;
use azbuco\user\models\User;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Basic user management functions
 */
class ManageController extends Controller
{

    public $userClass = User::class;


    /**
     * Create a user with the given parameters
     *
     * @param string $email The user email address.
     * @param string $password Password for the user.
     * @param string $name The user real name.
     * @return int The status of the action.
     */
    public function actionNew($email, $password, $name)
    {
        $transaction = Yii::$app->db->beginTransaction();
        $user = $this->createUser($email, $name, $password);
        if (!$user) {
            $transaction->rollBack();
            return 0;
        }

        $transaction->commit();
        $this->stdout('User created.' . "\n", Console::FG_GREEN);
    }

    /**
     * Change the password for the user.
     *
     * @param string $email Email address for the user
     * @param string $newPassword Note: there is no validation on the password, so don't be stupid
     */
    public function actionPasswordReset($email, $newPassword)
    {
        $user = $this->userClass::findByEmail($email);
        if (!$user) {
            $this->stdout('No user found with the email.' . "\n", Console::FG_RED);
        }
        $user->setPassword($newPassword);
        $user->updateAttributes(['password_hash']);
        $this->stdout('Password changed.' . "\n", Console::FG_GREEN);
    }

    /**
     * Promote user to role
     *
     * @param $email the user email address
     * @param $role
     */
    public function actionPromote($email, $role)
    {
        $user = $this->userClass::findByEmail($email);
        if (!$user) {
            $this->stdout('No user found with the email.' . "\n", Console::FG_RED);
            return;
        }

        Yii::$app->authManager->revokeAll($user->id);

        if ($this->assignRole($user, $role)) {
            $this->stdout('User is promoted' . "\n", Console::FG_GREEN);
        }
    }

    /**
     * List users
     *
     * @param null $search filer users by LIKE on name or email
     */
    public function actionList($search = null)
    {
        $userQuery = $this->userClass::find()
            ->active()
            ->andFilterWhere(['LIKE', 'name', $search])
            ->andFilterWhere(['LIKE', 'email', $search])
            ->orderBy(['name' => SORT_ASC]);

        $index = 0;
        foreach ($userQuery->each() as $user) {
            /* @var $user User */
            $this->stdout($user->name . ': ' . $user->email . "\n");
            $index++;
        }

        if ($index === 0) {
            $this->stdout("No users found.\n", Console::FG_RED);
        } else {
            $this->stdout("$index users found.\n", Console::FG_GREEN);
        }
    }

    /**
     * Add an access token to a user
     *
     * @param $email the token generated to this user
     * @param null $token if null, a random token generated
     * @param null $name name of the token, optional
     * @throws \yii\base\Exception
     */
    public function actionAddToken($email, $token = null, $name = null) {
        $user = $this->userClass::findByEmail($email);
        if (!$user) {
            $this->stdout('No user found with the email.' . "\n", Console::FG_RED);
            return;
        }

        if ($name === null) {
            $name = 'Generated from console command.';
        }

        if ($token === null) {
            $token = Yii::$app->security->generateRandomString();
        }

        $model = new AccessToken();
        $model->user_id = $user->id;
        $model->token = $token;
        $model->name = $name;

        if ($model->save()) {
            $this->stdout('Access token generated: ' . $token . "\n", Console::FG_GREEN);
        } else {
            $this->stdout('Unable to generate token.' . "\n", Console::FG_RED);
        }
    }

    protected function createUser($email, $name, $password)
    {
        $count = $this->userClass::find()
            ->where(['email' => $email])
            ->count();

        if ($count > 0) {
            $this->stdout('A user with the email already exists.' . "\n", Console::FG_RED);
            return false;
        }

        $user = new $this->userClass;
        $user->email = $email;
        $user->name = $name;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->registration_ip = $_SERVER['REMOTE_ADDR'] ?? 'console';
        $user->generateEmailConfirmationToken();
        $user->confirmed_at = time();

        if (!$user->save()) {
            foreach ($user->getFirstErrors() as $key => $error) {
                $this->stdout(' - ' . $key . ': ' . $error . "\n", Console::FG_RED);
            }
            return false;
        }

        return $user;
    }

    protected function assignRole(User $user, $role)
    {
        $auth = Yii::$app->getAuthManager();
        if ($auth === false) {
            $this->stdout('Cannot assign role "' . $role . '" as the AuthManager is not configured on your console application.' . "\n",
                Console::FG_RED);
            return false;
        } else {
            $userRole = $auth->getRole($role);
            if (null === $userRole) {
                $this->stdout('Role "' . $role . '" not found. Creating it.' . "\n", Console::FG_GREEN);
                $userRole = $auth->createRole($role);
                $userRole->description = $role;
                $auth->add($userRole);
            }
            $auth->assign($userRole, $user->id);
            return true;
        }
    }

}
