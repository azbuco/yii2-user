<?php

use azbuco\user\models\User;
use azbuco\user\UserModule;
use azbuco\user\tests\fixtures\UserFixture;

class PasswordStrengthTest {

//    protected $_formId = '#form-signup';
//
//    /**
//     * @var Module
//     */
//    protected $_module;
//
//    public function _fixtures()
//    {
//        return [
//            'partner' => [
//                'class' => UserFixture::class,
//            ],
//        ];
//    }
//
//    public function _before(FunctionalTester $I)
//    {
//        $this->_module = Yii::$app->getModule('user');
//
//        $I->amOnRoute('user/registration/signup');
//    }
//
//    public function signupDisabled(FunctionalTester $I)
//    {
//        $this->_module->enableRegistration = false;
//        
//        $I->haveHttpHeader('Status Code', '404');
//    }
//
//    public function signupWithEmptyFields(FunctionalTester $I)
//    {
//        $I->see('Sign Up', '.btn');
//        $I->submitForm($this->_formId, []);
//        $I->seeValidationError('Name cannot be blank.');
//        $I->seeValidationError('Email cannot be blank.');
//        $I->seeValidationError('Password cannot be blank.');
//    }
//
//    public function signupWithWrongEmail(FunctionalTester $I)
//    {
//        $I->submitForm(
//        $this->_formId, [
//            'SignupForm[name]' => 'wrong',
//            'SignupForm[email]' => 'ttttt',
//            'SignupForm[password]' => 'tester_password',
//        ]
//        );
//        $I->dontSee('Name cannot be blank.', '.help-block');
//        $I->dontSee('Password cannot be blank.', '.help-block');
//        $I->see('Email is not a valid email address.', '.help-block');
//    }
//
//    public function signupSuccessfullyWithoutEmailConfirmationAndWithAutoLogin(FunctionalTester $I)
//    {
//        $this->_module->requireEmailConfirmation = false;
//        $this->_module->autoLoginAfterRegistration = true;
//
//        $I->submitForm($this->_formId, [
//            'SignupForm[name]' => 'tester1',
//            'SignupForm[email]' => 'tester1@example.com',
//            'SignupForm[password]' => 'tester1_password',
//        ]);
//        $I->seeRecord(User::class, [
//            'name' => 'tester1',
//            'email' => 'tester1@example.com',
//        ]);
//        $I->see('Logout (tester1)', 'form button[type=submit]');
//    }
//
//    public function signupSuccessfullyWithoutEmailConfirmationAndWithoutAutoLogin(FunctionalTester $I)
//    {
//        $this->_module->requireEmailConfirmation = false;
//        $this->_module->autoLoginAfterRegistration = false;
//
//        $I->submitForm($this->_formId, [
//            'SignupForm[name]' => 'tester2',
//            'SignupForm[email]' => 'tester2@example.com',
//            'SignupForm[password]' => 'tester2_password',
//        ]);
//        $I->seeRecord(User::class, [
//            'name' => 'tester2',
//            'email' => 'tester2@example.com',
//        ]);
//        $I->dontSee('Logout', 'form button[type=submit]');
//        $I->see('Sign In', 'form button[type=submit]');
//    }
//
//    public function signupWithEmailConfirmationWithoutAutoLogin(FunctionalTester $I)
//    {
//        $this->_module->requireEmailConfirmation = true;
//        $this->_module->autoLoginAfterRegistration = false;
//
//        $I->amGoingTo('sign up');
//        $I->submitForm($this->_formId, [
//            'SignupForm[name]' => 'tester3',
//            'SignupForm[email]' => 'tester3@example.com',
//            'SignupForm[password]' => 'tester3_password',
//        ]);
//        $I->seeRecord(User::class, [
//            'name' => 'tester3',
//            'email' => 'tester3@example.com',
//        ]);
//        $I->see('Hello tester3!');
//        $I->see('We have sent you a confirmation message to tester3@example.com.');
//
//        $I->amGoingTo('try to login without confirmation.');
//        $I->amOnRoute('user/security/login');
//        $I->submitForm('#form-signin', [
//            'LoginForm[email]' => 'tester3@example.com',
//            'LoginForm[password]' => 'tester3_password',
//        ]);
//        $I->seeValidationError('Not verified user. Resend confirmation email.');
//        
//        $user = User::findByEmail('tester3@example.com');
//  
//        $I->amGoingTo('confirm my identity');
//        $I->amOnRoute('/user/registration/confirmation', ['token' => $user->email_confirmation_token]);
//        $I->dontSee('Logout');
//        $I->see('Sign Up Complete.');
//        $I->see('This page is redirecting in 5 seconds...');
//        
//        $I->amGoingTo('check that i can log in');
//        $I->amOnRoute('user/security/login');
//        $I->submitForm('#form-signin', [
//            'LoginForm[email]' => 'tester3@example.com',
//            'LoginForm[password]' => 'tester3_password',
//        ]);
//        $I->see('Logout (tester3)');
//    }
//
//    public function signupWithEmailConfirmationWithAutoLogin(FunctionalTester $I)
//    {
//        $this->_module->requireEmailConfirmation = true;
//        $this->_module->autoLoginAfterRegistration = true;
//
//        $I->amGoingTo('sign up');
//        $I->submitForm($this->_formId, [
//            'SignupForm[name]' => 'tester4',
//            'SignupForm[email]' => 'tester4@example.com',
//            'SignupForm[password]' => 'tester4_password',
//        ]);
//        $I->seeRecord(User::class, [
//            'name' => 'tester4',
//            'email' => 'tester4@example.com',
//        ]);
//        $I->see('Hello tester4!');
//        $I->see('We have sent you a confirmation message to tester4@example.com.');
//
//        $I->amGoingTo('try to login without confirmation.');
//        $I->amOnRoute('user/security/login');
//        $I->submitForm('#form-signin', [
//            'LoginForm[email]' => 'tester4@example.com',
//            'LoginForm[password]' => 'tester4_password',
//        ]);
//        $I->seeValidationError('Not verified user. Resend confirmation email.');
//        
//        $user = User::findByEmail('tester4@example.com');
//  
//        $I->amGoingTo('confirm my identity');
//        $I->amOnRoute('/user/registration/confirmation', ['token' => $user->email_confirmation_token]);
//        $I->see('Logout (tester4)');
//        $I->see('Sign Up Complete.');
//        $I->see('This page is redirecting in 5 seconds...');
//    }
}
