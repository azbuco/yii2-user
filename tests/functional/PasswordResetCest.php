<?php

use azbuco\user\models\User;
use azbuco\user\UserModule;
use azbuco\user\tests\fixtures\UserFixture;

class PasswordResetCest
{

    protected $_formId = '#form-request-password-reset';

    /**
     * @var UserModule
     */
    protected $_module;

    public function _fixtures()
    {
        return [
            'partner' => [
                'class' => UserFixture::class,
            ],
        ];
    }

    public function _before(FunctionalTester $I)
    {
        $this->_module = Yii::$app->getModule('user');

        $this->_module->requireEmailConfirmation = false;

        $I->amOnRoute('user/registration/request-password-reset');
    }

    public function passwordResetRequestWithEmptyEmail(FunctionalTester $I)
    {
        $I->see('Request Password Reset');
        $I->see('Continue', '.btn');
        $I->submitForm($this->_formId, []);
        $I->seeValidationError('Email cannot be blank.');
    }

    public function passwordResetRequestWithInvalidEmail(FunctionalTester $I)
    {
        $I->see('Request Password Reset');
        $I->see('Continue', '.btn');
        $I->submitForm($this->_formId, [
            'RequestPasswordResetForm[email]' => 'notexists@example.com',
        ]);
        $I->seeValidationError('Email is invalid.');
        $I->see('Continue', '.btn');
    }

    public function passwordResetRequestWithoutConfirmedUser(FunctionalTester $I)
    {
        $I->amGoingTo('test when email confirmation is not required');
        $I->see('Request Password Reset');
        $I->see('Continue', '.btn');
        $I->submitForm($this->_formId, [
            'RequestPasswordResetForm[email]' => 'unconfirmed.user@example.com',
        ]);
        $I->see('We have sent you an email with reset instructions.');

        $this->_module->requireEmailConfirmation = true;

        $I->amGoingTo('test when email confirmation is required');
        $I->amOnRoute('user/registration/request-password-reset');
        $I->see('Request Password Reset');
        $I->see('Continue', '.btn');
        $I->submitForm($this->_formId, [
            'RequestPasswordResetForm[email]' => 'unconfirmed.user@example.com',
        ]);
        $I->seeValidationError('Not verified user. Resend confirmation email.');
    }

    public function passwordResetRequestWithConfirmedUser(FunctionalTester $I)
    {
        $this->_module->requireEmailConfirmation = false;

        $I->amGoingTo('test a confirmed user when email confirmation is not required');
        $I->see('Request Password Reset');
        $I->see('Continue', '.btn');
        $I->submitForm($this->_formId, [
            'RequestPasswordResetForm[email]' => 'confirmed.user@example.com',
        ]);
        $I->see('We have sent you an email with reset instructions.');

        $this->_module->requireEmailConfirmation = true;

        $I->amGoingTo('test a confirmed user when email confirmation is required');
        $I->amOnRoute('user/registration/request-password-reset');
        $I->see('Request Password Reset');
        $I->see('Continue', '.btn');
        $I->submitForm($this->_formId, [
            'RequestPasswordResetForm[email]' => 'confirmed.user@example.com',
        ]);
        $I->see('We have sent you an email with reset instructions.');

        $user = User::findByEmail('confirmed.user@example.com');

        $I->amGoingTo('change my password');
        $I->amOnRoute('user/registration/password-reset', ['token' => $user->password_reset_token]);
        $I->see('Save New Password', '.btn');
        $I->submitForm('#form-password-reset', [
            'PasswordResetForm[password]' => 'new password',
        ]);

        $I->amGoingTo('log in with new password');
        $I->amOnRoute('/user/security/login');
        $I->submitForm('#form-login', [
            'LoginForm[email]' => 'confirmed.user@example.com',
            'LoginForm[password]' => 'new password',
        ]);
        $I->see('Logout (Confirmed User)');
    }

}
