<?php

namespace frontend\tests\functional;

use azbuco\user\tests\fixtures\UserFixture;
use FunctionalTester;

class LoginCest
{
    protected $_formId = '#form-login';
    
    public function _fixtures()
    {
         return [
            'user' => [
                'class' => UserFixture::class,
            ],
        ];
    }

    public function _before(FunctionalTester $I)
    {
        $I->amOnRoute('/user/security/login');
    }

    public function checkEmpty(FunctionalTester $I)
    {
        $I->submitForm($this->_formId, []);
        $I->seeValidationError('Email cannot be blank.');
        $I->seeValidationError('Password cannot be blank.');
    }

    public function checkWrongPassword(FunctionalTester $I)
    {
        $I->submitForm($this->_formId, [
            'LoginForm[email]' => 'confirmed.user@example.com',
            'LoginForm[password]' => 'wrong',
        ]);
        $I->seeValidationError('Incorrect email or password.');
    }
    
    public function checkUnconfirmedLogin(FunctionalTester $I)
    {
        $I->submitForm($this->_formId, [
            'LoginForm[email]' => 'unconfirmed.user@example.com',
            'LoginForm[password]' => 'password2',
        ]);
        $I->seeValidationError('Not verified user. Resend confirmation email.');
    }
    
    public function checkBlockedLogin(FunctionalTester $I)
    {
        $I->submitForm($this->_formId, [
            'LoginForm[email]' => 'blocked.user@example.com',
            'LoginForm[password]' => 'pw-blocked',
        ]);
        $I->seeValidationError('Blocked email address. For further information, please contact an administrator.');
    }

    public function checkValidLogin(FunctionalTester $I)
    {
        $I->submitForm($this->_formId, [
            'LoginForm[email]' => 'confirmed.user@example.com',
            'LoginForm[password]' => 'password1',
        ]);
        $I->see('Logout (Confirmed User)', 'form button[type=submit]');
        $I->dontSeeLink('Login');
        $I->dontSeeLink('Registration');
    }
}
