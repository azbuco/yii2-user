<?php

use azbuco\user\tests\fixtures\UserFixture;

class PasswordChangeCest
{

    protected $_formId = '#form-password-change';
    
    protected $_loginFormId = '#form-login';

    protected $_route = 'user/profile/password-change';

    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::class,
            ],
        ];
    }

    public function checkAccess(FunctionalTester $I)
    {
        $I->amOnRoute($this->_route);
        $I->seeResponseCodeIs(404);
    }

    public function checkEmpty(FunctionalTester $I)
    {
        $user = $I->grabFixture('user', 'confirmedUser');
        $I->amLoggedInAs($user);
        
        $I->amOnRoute($this->_route);

        $I->submitForm($this->_formId, []);
        $I->seeValidationError('Current Password cannot be blank.');
        $I->seeValidationError('New Password cannot be blank.');
        $I->seeValidationError('Repeat New Password cannot be blank.');
    }
    
    public function checkWrongPassword(FunctionalTester $I)
    {
        $user = $I->grabFixture('user', 'confirmedUser');
        $I->amLoggedInAs($user);
        
        $I->amOnRoute($this->_route);

        $I->submitForm($this->_formId, [
            'PasswordChangeForm[password_current]' => 'wrong password',
            'PasswordChangeForm[password_new]' => 'new password',
            'PasswordChangeForm[password_repeat]' => 'new password',
        ]);
        $I->seeValidationError('Incorrect password.');
    }
    
    public function checkRepeat(FunctionalTester $I)
    {
        $user = $I->grabFixture('user', 'confirmedUser');
        $I->amLoggedInAs($user);
        
        $I->amOnRoute($this->_route);

        $I->submitForm($this->_formId, [
            'PasswordChangeForm[password_current]' => 'password1',
            'PasswordChangeForm[password_new]' => 'new password',
            'PasswordChangeForm[password_repeat]' => 'new password notequal',
        ]);
        $I->seeValidationError('The passwords don\'t match.');
    }
    
    public function checkPasswordChange(FunctionalTester $I)
    {
        $user = $I->grabFixture('user', 'confirmedUser');
        $I->amLoggedInAs($user);
        
        $I->amOnRoute($this->_route);

        $I->submitForm($this->_formId, [
            'PasswordChangeForm[password_current]' => 'password1',
            'PasswordChangeForm[password_new]' => 'new password',
            'PasswordChangeForm[password_repeat]' => 'new password',
        ]);
        $I->see('Sign In');
        $I->submitForm($this->_loginFormId, [
            'LoginForm[email]' => 'confirmed.user@example.com',
            'LoginForm[password]' => 'new password',
        ]);
        $I->see('Logout (Confirmed User)');
    }

}
