<?php

use azbuco\user\tests\fixtures\UserFixture;

class ManageCest {

    protected $_formId = '#form-password-change';
    protected $_loginFormId = '#form-login';
    protected $_route = 'user/profile/password-change';

    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::class,
            ],
        ];
    }

    public function checkAccess(FunctionalTester $I)
    {
        $I->amOnRoute('/user/manage/index');
        $I->seeResponseCodeIs(404);

        $I->amOnRoute('/user/manage/create');
        $I->seeResponseCodeIs(404);

        $I->amOnRoute('/user/manage/update', ['id' => 1]);
        $I->seeResponseCodeIs(404);
        
        $admin = $I->grabFixture('user', 'admin');
        $I->amLoggedInAs($admin);
        
        $I->amOnRoute('/user/manage/index');
        $I->seeResponseCodeIs(200);
        
        $I->amOnRoute('/user/manage/create');
        $I->seeResponseCodeIs(200);

        $I->amOnRoute('/user/manage/update', ['id' => 1]);
        $I->seeResponseCodeIs(200);
    }

    public function checkIndex(FunctionalTester $I)
    {
        $admin = $I->grabFixture('user', 'admin');
        $I->amLoggedInAs($admin);

        $I->amOnRoute('/user/manage/index');
        $I->see('Confirmed User');
        $I->see('confirmed.user@example.com');
        $I->see('Unconfirmed User');
        $I->see('unconfirmed.user@example.com');
    }

    public function checkCreateEmpty(FunctionalTester $I)
    {
        $admin = $I->grabFixture('user', 'admin');
        $I->amLoggedInAs($admin);

        $I->amOnRoute('/user/manage/create');

        $I->submitForm('#form-user', []);
        $I->seeValidationError('Name cannot be blank.');
        $I->seeValidationError('Email cannot be blank.');
        $I->seeValidationError('Password cannot be blank.');
    }

    public function checkCreate(FunctionalTester $I)
    {
        $admin = $I->grabFixture('user', 'admin');
        $I->amLoggedInAs($admin);

        $I->amOnRoute('/user/manage/create');

        $I->submitForm('#form-user', [
            'UserForm[name]' => 'Test User',
            'UserForm[email]' => 'testuser@example.com',
            'UserForm[password]' => 'password',
        ]);
        $I->see('Test User');
        $I->see('testuser@example.com');
    }
    
    public function checkBlock(FunctionalTester $I) {
        $admin = $I->grabFixture('user', 'admin');
        $I->amLoggedInAs($admin);

        $I->amOnRoute('/user/manage/view', ['id' => 1]);
        $I->see('Block User', '.btn-danger');
        $I->dontSee('Unblock User', '.btn-primary');
        
        $url = \yii\helpers\Url::to(['/user/manage/block', 'id' => 1]);
        $I->sendAjaxPostRequest($url);
        $user = $I->grabRecord(azbuco\user\models\User::class, ['id' => 1]);
        $I->assertNotNull($user->blocked_at);
        
        $I->amOnRoute('/user/manage/view', ['id' => 1]);
        $I->dontSee('Block User', '.btn-danger');
        $I->see('Unblock User', '.btn-primary');
        
        $url = \yii\helpers\Url::to(['/user/manage/block', 'id' => 1, 'block' => false]);
        $I->sendAjaxPostRequest($url);
        $user = $I->grabRecord(azbuco\user\models\User::class, ['id' => 1]);
        $I->assertNull($user->blocked_at);
        
        $I->amOnRoute('/user/manage/view', ['id' => 1]);
        $I->see('Block User', '.btn-danger');
        $I->dontSee('Unblock User', '.btn-primary');
    }

    public function checkDelete(FunctionalTester $I) {
        $admin = $I->grabFixture('user', 'admin');
        $I->amLoggedInAs($admin);

        $I->amOnRoute('/user/manage/view', ['id' => 1]);
        $I->see('Delete User', '.btn-danger');
        $user = $I->grabRecord(azbuco\user\models\User::class, ['id' => 1]);
        $I->assertNull($user->deleted_at);
        
        $url = \yii\helpers\Url::to(['/user/manage/delete', 'id' => 1]);
        $I->sendAjaxPostRequest($url);
        $user = $I->grabRecord(azbuco\user\models\User::class, ['id' => 1]);
        $I->assertNotNull($user->deleted_at);
        
        $I->amOnRoute('/user/manage/view', ['id' => 1]);
        $I->dontSee('Delete User');
    }
}
