<?php

use azbuco\user\models\User;
use azbuco\user\tests\fixtures\UserFixture;
use yii\helpers\Url;

class SignupCest {

    protected $formId = '#form-signup';

    public function _fixtures()
    {
        return [
            'partner' => [
                'class' => UserFixture::class,
            ],
        ];
    }

    public function _before(AcceptanceTester $I)
    {
//        \Yii::$container->set(\azbuco\user\Module::className(), [
//            'enableEmailConfirmation' => false,
//            'autoLoginAfterRegistration' => true,
//        ]);
        
        var_dump(Yii::$app->name);
        exit;
        
       
        
        $I->amOnPage(Url::to(['/user/registration/signup']));
    }

    public function signupWithEmptyFields(AcceptanceTester $I)
    {
        $I->see('Signup');
        $I->submitForm($this->formId, []);
        $I->seeValidationError('Name cannot be blank.');
        $I->seeValidationError('Email cannot be blank.');
        $I->seeValidationError('Password cannot be blank.');
    }

    public function signupWithWrongEmail(AcceptanceTester $I)
    {
        $I->submitForm(
        $this->formId, [
            'SignupForm[name]' => 'tester',
            'SignupForm[email]' => 'ttttt',
            'SignupForm[password]' => 'tester_password',
        ]
        );
        $I->dontSee('Name cannot be blank.', '.help-block');
        $I->dontSee('Password cannot be blank.', '.help-block');
        $I->see('Email is not a valid email address.', '.help-block');
    }

    public function signupSuccessfullyWithoutConfirmation(AcceptanceTester $I)
    {
        $module = Yii::$app->getModule('user')->enableEmailConfirmation;
        
        var_dump($module->autoLoginAfterRegistration);
        
        /* @var $module \azbuco\user\UserModule */

        $module->requireEmailConfirmation = false;
        $module->autoLoginAfterRegistration = true;
        
        var_dump($module->autoLoginAfterRegistration);
        
        $I->wait(5);
       
        
        $I->submitForm($this->formId, [
            'SignupForm[name]' => 'tester',
            'SignupForm[email]' => 'tester.email@example.com',
            'SignupForm[password]' => 'tester_password',
        ]);
        
        var_dump($module->autoLoginAfterRegistration);
        exit;

        $I->seeRecord(User::class, [
            'name' => 'tester',
            'email' => 'tester.email@example.com',
        ]);

        $I->see('Logout (tester)', 'form button[type=submit]');
    }
    
   

}
