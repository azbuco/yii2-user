<?php

namespace azbuco\user\tests\fixtures;

class UserFixture extends \yii\test\ActiveFixture {

    public function init()
    {
        $this->modelClass = \azbuco\user\models\User::class;

        parent::init();
    }

}
