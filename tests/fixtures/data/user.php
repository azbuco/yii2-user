<?php

return [
    'confirmedUser' => [
        'id' => 1,
        'name' => 'Confirmed User',
        'email' => 'confirmed.user@example.com',
        'auth_key' => \Yii::$app->security->generateRandomString(),
        'password_hash' => \Yii::$app->security->generatePasswordHash('password1'),
        'email_confirmation_token' => 'token-1',
        'confirmed_at' => time('2000-01-01 00:00:00'),
        'updated_at' => time('2000-01-01 00:00:00'),
        'created_at' => time('2000-01-01 00:00:00'),
    ],
    'unconfirmedUser' => [
        'id' => 2,
        'name' => 'Unconfirmed User',
        'email' => 'unconfirmed.user@example.com',
        'auth_key' => \Yii::$app->security->generateRandomString(),
        'password_hash' => \Yii::$app->security->generatePasswordHash('password2'),
        'email_confirmation_token' => 'token-2',
        'updated_at' => time('2000-01-01 00:00:00'),
        'created_at' => time('2000-01-01 00:00:00'),
    ],
    'admin' => [
        'id' => 3,
        'name' => 'Admin User',
        'email' => 'admin.user@example.com',
        'auth_key' => \Yii::$app->security->generateRandomString(),
        'password_hash' => \Yii::$app->security->generatePasswordHash('password3'),
        'email_confirmation_token' => 'token-3',
        'confirmed_at' => time('2000-01-01 00:00:00'),
        'updated_at' => time('2000-01-01 00:00:00'),
        'created_at' => time('2000-01-01 00:00:00'),
    ],
    'blockedUser' => [
        'id' => 4,
        'name' => 'Blocked User',
        'email' => 'blocked.user@example.com',
        'auth_key' => \Yii::$app->security->generateRandomString(),
        'password_hash' => \Yii::$app->security->generatePasswordHash('pw-blocked'),
        'email_confirmation_token' => 'token-4',
        'confirmed_at' => time('2000-01-01 00:00:00'),
        'updated_at' => time('2000-01-01 00:00:00'),
        'created_at' => time('2000-01-01 00:00:00'),
        'blocked_at' => time('2000-01-01 00:00:00'),
    ],
];

