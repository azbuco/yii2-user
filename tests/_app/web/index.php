<?php

// NOTE: Make sure this file is not accessible when deployed to production
if (!in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
    die('You are not allowed to access this file.');
}

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');

// Search for autoload, since performance is irrelevant and usability isn't!
$dir = __DIR__ . '/..';
while (!file_exists($dir . '/vendor/autoload.php')) {
    if ($dir == dirname($dir)) {
        throw new \Exception('Failed to locate autoload.php');
    }
    $dir = dirname($dir);
}

defined('VENDOR_DIR') or define('VENDOR_DIR', $dir . '/vendor');

require_once VENDOR_DIR . '/autoload.php';
require VENDOR_DIR . '/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/test.php';

(new yii\web\Application($config))->run();
