<?php

return [
    'id' => 'azbuco-user-tests',
    'basePath' => dirname(__DIR__),
    'language' => 'en-US',
    'name' => 'User test',
    'aliases' => [
        '@tests' => dirname(dirname(__DIR__)),
        '@vendor' => VENDOR_DIR,
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@azbuco/user' => '@vendor/azbuco/yii2-user',
    ],
    'modules' => [
        'user' => [
            'class' => azbuco\user\UserModule::class,
        ],
    ],
    'components' => [
        'assetManager' => [
            'basePath' => dirname(__DIR__) . '/assets',
        ],
        'authManager' => [
            //'class' => 'yii\rbac\DbManager',
            'class' => 'yii\rbac\PhpManager',
            'itemFile' => '@tests/fixtures/rbac/items.php',
            'assignmentFile' => '@tests/fixtures/rbac/assignments.php',
            'ruleFile' => '@tests/fixtures/rbac/rules.php',
        ],
        'db' => require __DIR__ . '/db.php',
        'mailer' => [
            'useFileTransport' => true,
            'htmlLayout' => '@vendor/azbuco/yii2-user/mail/layouts/html',
            'textLayout' => '@vendor/azbuco/yii2-user/mail/layouts/text',
        ],
        'request' => [
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
        ],
        'user' => [
            'identityClass' => azbuco\user\models\User::class, // User must implement the IdentityInterface
            'enableAutoLogin' => true,
            'loginUrl' => ['user/sercurity/login'],
        ],
    ],
    'params' => [],
];
