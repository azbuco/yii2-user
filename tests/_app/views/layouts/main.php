<?php

use yii\helpers\Html;

if (Yii::$app->user->getIsGuest()) {
    echo Html::a('Login', ['/user/security/login']);
    echo " ";
    echo Html::a('Registration', ['/user/registration/signup']);
} else {
    echo Html::beginForm(['/user/security/logout'], 'post')
    . Html::submitButton('Logout (' . Yii::$app->user->identity->name . ')', ['class' => 'btn btn-link logout'])
    . Html::endForm();
}

echo '<hr />';

foreach(Yii::$app->session->getAllFlashes() as $flash) {
    var_dump($flash);
}

echo $content;
